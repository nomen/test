<?php

class Cron_Checker {
    
    /**
     * Try to check that post exists and email address was not changed
     * 
     * Return value:
     * 0 - not exists (404)
     * 1 - not correspond with post email
     * 
     * @param mixed $qmail post data
     * @return int result code
     */
    public function checkPostExistance($qmail)
    {
//        $data = $this->getData('http://toronto.en.craigslist.ca/tor/sof/4103025775.html');
        $data = $this->getData($qmail['link']);
        if (!$data) {
            return 0;
        }
        
        // check post expiration
        $matches = null;
        preg_match_all("/(Page Not Found|404 Error|deleted by its author|has been deleted|posting has expired)/ism", $data['output'], $matches, PREG_SET_ORDER);
        $notExists404 = (isset($matches[0][1])) ? trim(strip_tags($matches[0][1])) : '';
        
        if ($notExists404) {
            return 0;
        }
        
        $reply_email = null;
        
        // check that we should get email from separate page
        $matchesCustom = null;
        preg_match_all("/<a\s*href=\"([\/]?reply.*?)\"/ism", $data['output'], $matchesCustom, PREG_SET_ORDER);
        $reply_email_page = (isset($matchesCustom[0][1])) ? trim(strip_tags($matchesCustom[0][1])) : '';
        
        if ($reply_email_page) {
            
            $domain = parse_url($qmail['link'], PHP_URL_HOST);
            $subData = $this->getData($domain.$reply_email_page);
            
            $matchesCustom = null;
            preg_match_all("/class=\"mailto\"[^>]*>(.*?)<\/a>/ism", $subData['output'], $matchesCustom, PREG_SET_ORDER);
            $reply_email = (isset($matchesCustom[0][1])) ? trim(strip_tags($matchesCustom[0][1])) : '';
        }
        else {
            
            // get reply email according to old slicing
            $matchesCustom = null;
            preg_match_all("/mailto\:[^>]*>(.*?)</ism", $data['output'], $matchesCustom, PREG_SET_ORDER);
            $reply_email = (isset($matchesCustom[0][1])) ? trim(strip_tags($matchesCustom[0][1])) : '';
        }
        
        if (!$reply_email) {
            return 1;
        }
        else if ($reply_email != $qmail['reply_email']) {
            return 1;
        }
        
        return -1;
    }
    
    private function getData($url)
    {
        $retries = 2;
        $result = false;
        $response = null;
        
        $curl = curl_init($url);

        if (is_resource($curl) === true) {
            
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
            curl_setopt($curl, CURLOPT_FAILONERROR, true);
            curl_setopt($curl, CURLOPT_ENCODING, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept-Encoding: gzip,deflate'));

            while (($result === false) && (--$retries > 0)) {
                
                sleep(1);
                $result = curl_exec($curl);
                $response = curl_getinfo($curl);
            }
            
            curl_close($curl);
        }
        
        switch($response['http_code'])
        {
            case 200:
                $return = array(
                    'response' =>$response,
                    'output'   =>$result
                );
            break;
            
            case 301: case 302:
                
                foreach (get_headers($response['url']) as $value) {
                    if (substr(strtolower($value), 0, 9) == "location:") {
                        return $this->getData(trim(substr($value, 9, strlen($value))));
                    }
                }
            break;
            
            default:
                $return = false;
        }

        return $return;
    }
}
