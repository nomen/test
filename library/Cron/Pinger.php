<?php

class Cron_Pinger {
    
    private $db = null;
    private $sentOrderId = null;
    private $logger = null;
    
    public function __construct($dbAdapter = null)
    {
        if ($dbAdapter) {
            $this->db = $dbAdapter;
        }
        else {
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
            $this->db = new Zend_Db_Adapter_Mysqli($config->resources->db->params);
        }
    }
    
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
    
    public function resetSentPingTplTable()
    {
        $this->logger->logAction('resetSentPingTplTable');
        $this->db->query('TRUNCATE craiglist.sent_ping_tpl');
        
        $templates = $this->db->select()->from('templates')->query()->fetchAll();
        foreach ($templates as $tpl) {
            $this->db->insert('sent_ping_tpl', array('template_id' => $tpl['id'], 'modified' => date('Y-m-d H:i:s')));
        }
    }
    
    private function getTpl($tpl_candidat)
    {
        $this->logger->logAction('getTpl');
        
        $tpl = $this->db->select()->from('templates')->where('id = ?', $tpl_candidat['template_id'])->query()->fetch();
        if (!$tpl) {
            
            $this->logger->logAction('getTpl: not found');
            return null;
        }
        
        $tpl['in_progress'] = $tpl_candidat['in_progress'];
        return $tpl;
    }
    
    private function updateSentPingTpl($template_id)
    {
        $this->logger->logAction('updateSentPingTpl', $template_id);
        
        $this->db->update(
            'sent_ping_tpl',
            array(
                'in_progress' => new Zend_Db_Expr('in_progress + 1'),
                'modified' => date('Y-m-d H:i:s')),
            array('template_id = ?' => $template_id)
        );
    }
    
    public function getTplForPing()
    {
        $this->logger->logAction('getTplForPing');
        
        // find template that is already in ping progress
        $tpl_candidat = $this->db->select()
                                ->from('sent_ping_tpl')
                                ->where('in_progress <> ?', '0')
                                ->where('in_progress <> ?', '4')
                                ->order('modified')
                                ->limit(1)->query()->fetch();
        if ($tpl_candidat) {
            
            $this->updateSentPingTpl($tpl_candidat['template_id']);
            return $this->getTpl($tpl_candidat);
        }
        
        // try to find template that was not used yet
        // or used rarely than others
        $tpl_candidat = $this->db->fetchRow("
            SELECT *
            FROM sent_ping_tpl
            WHERE used_cnt = (SELECT MIN(used_cnt) FROM sent_ping_tpl)
                AND in_progress <> '4'
            ORDER BY id
            LIMIT 1
        ");
        
        if ($tpl_candidat) {
            
            $this->updateSentPingTpl($tpl_candidat['template_id']);
            return $this->getTpl($tpl_candidat);
        }
        
        // find template that was sent more than 10 times for all time
        $tpl_candidat = $this->db->select()
                                ->from('sent_ping_tpl')
                                ->where('used_cnt >= 10')
                                ->where('in_progress <> ?', '4')
                                ->order('modified')
                                ->limit(1)
                                ->query()->fetch();
        if ($tpl_candidat) {
            
            $this->updateSentPingTpl($tpl_candidat['template_id']);
            return $this->getTpl($tpl_candidat);
        }
        
        // try to find most used template for last 10 sent
        $tpl_candidat = $this->db->fetchRow('
            SELECT lastSent.template_id, count(1) AS cnt
            FROM (
                SELECT *
                FROM craiglist.mail_queue
                WHERE
                        template_id IS NOT NULL
                    AND sent_status = "success"
                ORDER BY id desc
                LIMIT 10
                ) AS lastSent

            GROUP BY lastSent.template_id
            ORDER BY cnt DESC
            LIMIT 1
        ');
        
        if ($tpl_candidat) {
            
            $this->updateSentPingTpl($tpl_candidat['template_id']);
            $tpl_candidat['in_progress'] = $this->getTplProgress($tpl_candidat['template_id']);
            return $this->getTpl($tpl_candidat);
        }
        
        return null;
    }
    
    private function getTplProgress($template_id)
    {
        $this->logger->logAction('getTplProgress', $template_id);
        return $this->db->select()->from('sent_ping_tpl', array('in_progress'))->where('template_id = ?', $template_id)->query()->fetch();
    }
    
    public function getTplInProgress()
    {
        $this->logger->logAction('getTplInProgress');
        return $this->db->select()->from('sent_ping_tpl')->where('in_progress <> ?', '0')->where('in_progress <> ?', '4')->query()->fetch();
    }
    
    private function customShuffle($what)
    {
        $this->logger->logAction('customShuffle');
        
        $replaceIndex = count($what)/2;
        $tmp = $what[0];
        $what[0] = $what[$replaceIndex];
        $what[$replaceIndex] = $tmp;
        
        return $what;
    }
    
    private function initSentOrder()
    {
        $this->logger->logAction('initSentOrder');
        
        // try to get last record info
        $last = $this->db->select()->from('sent_ping_order')->order('id DESC')->limit(1)->query()->fetch();
        $checkpointEmails = $this->db->select()->from('checkpoints', array('id'))->query()->fetchAll();
        shuffle($checkpointEmails);
        
        if ($last && $last['checkpoint_id'] == $checkpointEmails[0]['id']) {
            $checkpointEmails = $this->customShuffle($checkpointEmails);
        }
        
        $this->db->query('TRUNCATE craiglist.sent_ping_order');
        
        foreach ($checkpointEmails as $xCpId => $xCheckpoint) {
            $this->db->insert('sent_ping_order', array('checkpoint_id' => $xCheckpoint['id']));
        }
    }
    
    public function getNextCheckpoint()
    {
        $this->logger->logAction('getNextCheckpoint');
        
        return $this->db->select()
                    ->from(array('spo' => 'sent_ping_order'))
                    ->join(array('cp' => 'checkpoints'), 'cp.id = spo.checkpoint_id', array('reply_email', 'subject', 'checkpoint_id' => 'id', 'link'))
                    ->where('spo.used = ?', '0')
                    ->order('spo.id ASC')
                    ->limit(1)->query()->fetch();
    }
    
    public function getPingTo()
    {
        $this->logger->logAction('getPingTo');
        $nextCheckpoint = $this->getNextCheckpoint();
        
        if (!$nextCheckpoint) {
            
            $this->initSentOrder();
            $nextCheckpoint = $this->getNextCheckpoint();
        }
        
        $this->sentOrderId = $nextCheckpoint['checkpoint_id'];
        return $nextCheckpoint;
    }
    
    public function updateSentOrder()
    {
        $this->logger->logAction('updateSentOrder');
        
        if (!$this->sentOrderId) {
            $tmpVal = $this->db->select()
                                        ->from('sent_log', array('checkpoint_id'))
                                        ->where('ping = ?', '1')
                                        ->order('id DESC')
                                        ->limit(1)
                                        ->query()->fetch();
            $this->sentOrderId = $tmpVal['checkpoint_id'];
        }
        
        $this->db->update(
            'sent_ping_order',
            array('used' => '1'),
            array('checkpoint_id = ?' => $this->sentOrderId)
        );
    }
    
    public function updateSentLog($host_info, $template_id, $checkpoint_id)
    {
        $this->logger->logAction('updateSentLog');
        
        $this->db->insert('sent_log', array(
            'host_id' => $host_info['host_id'],
            'account_id' => $host_info['account_id'],
            'template_id' => $template_id,
            'sent_date' => date('Y-m-d H:i:s'),
            'ping' => '1',
            'checkpoint_id' => $checkpoint_id
        ));
    }
    
    public function checkCheckpointData($data)
    {
        $checker = new Cron_Checker();
        $checkRes = $checker->checkPostExistance($data);
        if ($checkRes != -1) {
            
            mail(
                'imaksymenko@webdevs.com', //'llitvin@webdevs.com',
                'Cron.Pinger.Error',
                "Инфо:\n"
                . (($checkRes)
                        ? 'Email-адрес в посте для пинга был изменен (у нас в базе - '.$data['reply_email'].')'."\n".$data['link']
                        : 'Пост для пинга судя по всему заэкспайрился!'."\n".$data['link'])
            );
            
            $this->logger->logAction('checkCheckpointData', 'ERROR');
            return false;
        }
        
        $this->logger->logAction('checkCheckpointData', 'OK');
        return true;
    }
    
    public function sendPingMail($tpl)
    {
        $this->logger->logAction('sendPingMail');
        $sendres = true;
        
        // select server to send from
        $mailer = new Cron_Mailer();
        $mailer->setLogger($this->logger);
        $host = $mailer->getSendParameters();
        
        $msgBody = $mailer->prepareMessageBody($tpl['content']);
        $pData = $this->getPingTo();
        
        if (!$this->checkCheckpointData($pData)) {
            return false;
        }
        
        $subject = "----- {$pData['subject']} -----";
        
// HARDCODED <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        $emailTo = $pData['reply_email'];
//        $emailTo = 'imaksymenko@webdevs.com';
        
        // send
        try {
            $sendres = $mailer->doSendViaXSMTP($host, $emailTo, $subject, $msgBody);
        }
        catch (Exception $ex) {
            
            sleep(10);
            try {
                $sendres = $mailer->doSendViaXSMTP($host, $emailTo, $subject, $msgBody);
            }
            catch (Exception $ex) {
                
                $sendres = false;
                $this->logger->logSendMailCommon('PING ERROR !!!');
                $this->logger->log('!!!!! ERROR !!!!! While trying to send PING email to '.$emailTo);
                $this->logger->log("{$ex->getMessage()}\n\n{$ex->getFile()}\n\n{$ex->getTraceAsString()}");
                $this->logger->logAction('sendPingMail: !!!!! ERROR !!!!! While trying to send PING email to '.$emailTo);
                $this->logger->logAction("sendPingMail: {$ex->getMessage()}\n\n{$ex->getFile()}\n\n{$ex->getTraceAsString()}");
                
                print_r($ex->getMessage());
                print_r($ex->getCode());
                print_r($ex->getFile());
                print_r($ex->getTraceAsString());
            }
        }
        
        $mailer->updateSentOrder();
        $this->updateSentOrder();
        $this->updateSentLog($host, $tpl['id'], $pData['checkpoint_id']);
        
        return $sendres;
    }
    
    public function getLastPingLog()
    {
        $this->logger->logAction('getLastPingLog');
        
        $result = $this->db->select()
                    ->from(array('sl' => 'sent_log'), array('host_id', 'account_id', 'template_id'))
                    ->join(array('cp' => 'checkpoints'), 'cp.id = sl.checkpoint_id', array('reply_email', 'subject', 'account_login', 'account_pwd'))
                    ->where('ping = ?', '1')
                    ->order('sl.id desc')
                    ->limit(1)
                    ->query()->fetch();
        
//        print_r($result->__toString()); die;
        
        return $result;
    }
    
    private function checkPing($mHeaders, $subject)
    {
        $this->logger->logAction('checkPing');
        
        $fromOK = false;
        $subjectOK = false;
        
        $mailer = new Cron_Mailer();
        $mailer->setLogger($this->logger);
        
        // check FROM
        foreach ($mailer->accounts as $acc) {
            
            if (preg_match('/('.$acc['user_name_from'].')/ism', $mHeaders['from'])) {
                
                $fromOK = true;
                break;
            }
        }
        
        // check SUBJECT
        if (preg_match('/(-----\s*'.$subject.'\s*-----)/ism', $mHeaders['subject'])) {
            $subjectOK = true;
        }
        
        $this->logger->logAction('checkPing: results', (($fromOK && $subjectOK) ? 'true' : 'false'));
        return ($fromOK && $subjectOK) ? true : false;
    }
    
    public function receivePingEmail()
    {
        $this->logger->logAction('receivePingEmail');
        
        // get info about last send params
        $sender = $this->getLastPingLog();
//        var_dump($sender);
//        $sender['account_login'] = 'imaksymenko@webdevs.com';
//        $sender['account_pwd'] = '*KrUbs9)m';
        
        $imapHost = ($sender['account_login'] == 'ivanmokrenko@yandex.ru') ? 'imap.yandex.ru' : 'imap.gmail.com';
        
//        print_r($sender);
//        print_r($imapHost); echo PHP_EOL;
//        die;
        
        $mail = null;
        try {
            $mail = new Zend_Mail_Storage_Imap(array(
                'host'     => $imapHost,
                'user'     => $sender['account_login'],
                'password' => $sender['account_pwd'],
                'port'     => '993',
                'ssl'      => true,
                'auth'	   => 'login'
            ));
        }
        catch (Exception $ex) {
            
            print_r($ex->getMessage());
            print_r($ex->getCode());
            print_r($ex->getFile());
            print_r($ex->getTraceAsString());
            
            $this->logger->logAction($ex->getMessage());
            $this->logger->logAction($ex->getCode());
            $this->logger->logAction($ex->getFile());
            $this->logger->logAction($ex->getTraceAsString());
            return false;
        }
        
//        print_r($mail->countMessages());
//        echo PHP_EOL;
//        die;
        
        $mCnt = $mail->countMessages();
        $flagOK = false;
        
        // try to find our ping in last 20 messages
        // this is needed because of other craigslist users that also can send messages to our test ads
        $lastMsgID = ((($mCnt)-20) <= 0) ? 0 : (($mCnt)-20);
        
        for ($id = $mCnt; $id > $lastMsgID; $id--) {
            
            $message = $mail->getMessage($id);
            
            if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) {
                continue;
            }
            
            $mHeaders = $message->getHeaders();
            if ($this->checkPing($mHeaders, $sender['subject'])) {
                
                $flagOK = true;
                $mail->setFlags($id, array(Zend_Mail_Storage::FLAG_SEEN));
                break;
            }
        }
        
//        var_dump($flagOK);
//        die;
        
        $this->logger->logAction('receivePingEmail: results', ($flagOK ? 'true' : 'false'));
        return $flagOK;
    }
    
    public function markNextLevelBan($pTpl)
    {
        $this->logger->logAction('markNextLevelBan');
        
        // mark custom template as used in pinger process
        // increase in_progress for sent_ping_tpl
        $this->updateSentPingTpl($pTpl['template_id']);
        
        // change template status
        $ban_status  = array(
            0 => 'clean',
            1 => 'preban1',
            2 => 'preban2',
            3 => 'ban'
        );
        $this->db->update(
            'templates',
            array(
                'status' => $ban_status[$pTpl['in_progress']],
                'ban_date' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')),
            array('id = ?' => $pTpl['template_id'])
        );
        
        $this->logger->logAction('markNextLevelBan: #'.$pTpl['template_id'].' - '.$ban_status[$pTpl['in_progress']]);
    }
    
    public function doNextInProgressLevel($pTpl)
    {
        $this->logger->logAction('doNextInProgressLevel');
        
        $this->db->update(
            'sent_ping_tpl',
            array(
                'in_progress' => new Zend_Db_Expr('in_progress + 1'),
                'modified' => date('Y-m-d H:i:s')
            ),
            array('template_id = ?' => $pTpl['template_id'])
        );
    }
    
    public function updateTplBanStatus($pTpl)
    {
        $this->logger->logAction('updateTplBanStatus');
        
        $this->db->update('templates', array('status' => 'clean', 'modified' => date('Y-m-d H:i:s')), array('id = ?' => $pTpl['template_id']));
    }
    
    public function updateTplSentStats($pTpl)
    {
        $this->logger->logAction('updateTplSentStats');
        
        $this->db->update(
            'sent_ping_tpl',
            array(
                'used_cnt' => new Zend_Db_Expr('used_cnt + 1'),
                'in_progress' => '0',
                'modified' => date('Y-m-d H:i:s')
            ),
            array('template_id = ?' => $pTpl['template_id'])
        );
    }
}
