<?php

class Cron_Queue {
    
    private $db = null;
    private $logger = null;
    private $what = null;
    
    public function __construct($dbAdapter = null)
    {
        if ($dbAdapter) {
            $this->db = $dbAdapter;
        }
        else {
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
            $this->db = new Zend_Db_Adapter_Mysqli($config->resources->db->params);
        }
    }
    
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
    
    public function setWhat($what)
    {
        $this->logger->logAction('setWhat', $what);
        $this->what = $what;
    }
    
    private function prepareItem($mq_result)
    {
        $this->logger->logAction('prepareItem', $mq_result);
        
        if (!isset($mq_result['id']) || empty($mq_result['id'])) {
            return false;
        }
        
        // *************************************************************** //
        $mq_result['template_id'] = ($mq_result['template_id'] > 7) ? rand(1, 2) : $mq_result['template_id'];
        
        $result = array();
        $result['id'] = $mq_result['id'];
        $result['template_id'] = $mq_result['template_id'];
        $result['rkey'] = isset($mq_result['rkey']) ? $mq_result['rkey'] : null;
        
        $post = $this->db->select()
                    ->from('posts', array('reply_email', 'post_title', 'link', 'id'))
                    ->where('id = ?', $mq_result['post_id'])
                    ->query()->fetch();
        
        // if there was found email that we should proceed as not customized
        if ($mq_result['custom'] == '1') {
            
            $this->logger->logAction('prepareItem: it is customized item');
            
            $result['reply_email'] = $mq_result['custom_email'];
            $result['subject'] = preg_replace('/&#\d+;/', '', $mq_result['custom_subject']);
            $result['link'] = $post['link'];
            $result['post_id'] = $post['id'];
            $result['body'] = nl2br($mq_result['custom_body']);
            
            return $result;
        }
        
        $result['reply_email'] = $post['reply_email'];
        $result['subject'] = preg_replace('/&#\d+;/', '', $post['post_title']);
        $result['link'] = $post['link'];
        $result['post_id'] = $post['id'];
        
        if (isset($result['rkey']) && is_numeric($result['rkey']) && $this->what == ITS_REMINDER_TIME) {
            
            $tpl = $this->db->select()
                    ->from('reminders', array('content' => 'body'))
                    ->where('id = ?', $result['rkey'])
                    ->query()->fetch();
        }
        else {
            
            $tpl = $this->db->select()
                    ->from('templates')
                    ->where('id = ?', $mq_result['template_id'])
                    ->query()->fetch();
        }
        
        $result['body'] = nl2br($tpl['content']);
        
        return $result;
    }
    
    private function getNAMailerItem()
    {
        $this->logger->logAction('getNAMailerItem');
        
        return $this->db->fetchRow("
            SELECT mq.*
            FROM mail_queue as mq
                inner join posts as p on p.id = mq.post_id
            where 
                mq.sent_date IS NULL
                and (
                        (p.reply_email not like '%craigslist.org%' and mq.custom_email is null)
                    or (mq.custom_email is not null and mq.custom_email not like '%craigslist.org%')
                )
            order by mq.id desc
            limit 1
        ");
    }
    
    private function getMailerItem()
    {
        $this->logger->logAction('getMailerItem');
        
        $mq_result = $this->db->select()
                        ->from(array('mq' => 'mail_queue'))
                        ->join(array('p' => 'posts'), 'p.id = mq.post_id', array('reply_email', 'reply_email_real'))
                        ->join(array('t' => 'templates'), 't.id = mq.template_id', array('status'))
                        ->where('t.status = ?', 'clean')
                        ->where('mq.sent_date IS NULL')
                        ->where('p.reply_email like "%craigslist.org%" OR mq.custom_email like "%craigslist.org%"')
                        ->order('mq.id DESC')
                        ->limit(1)
                        ->query()->fetch();
        
//        var_dump($mq_result->__toString()); die('###');
            
        if (!$mq_result) {
            
            $this->logger->logAction('getMailerItem: not found any item');
            return null;
        }
        
        return $mq_result;
    }
    
    private function getReminderItem($anonymous=true)
    {
        $this->logger->logAction('getReminderItem', ($anonymous ? 'true' : 'false'));
        
        $mq_result = null;
        $reminders = null;
        $rkey = null;
        
        $reminders = $this->getReminderList(false);
        
        // try to get appropriate mail queue record
        $adReminder = $this->findAdToRemind($reminders, $anonymous);
        
        // if for all of reminders we don't found any appropriate post
        if (!$adReminder) {
            
            $this->logger->logAction('getReminderItem: not found item to remind');
            return null;
        }
        
        return $adReminder;
    }
    
    private function getReminderList($full=true)
    {
        $this->logger->logAction('getReminderList', ($full ? 'true' : 'false'));
        
        $custom_fields = (!$full) ? array('id', 'days') : '*';
        $result = $this->db->select()->from('reminders', $custom_fields)->query()->fetchAll();
        return $result;
    }
    
    private function findAdToRemind($reminders, $anonymous=true)
    {
        $this->logger->logAction('findAdToRemind', ($anonymous ? 'true' : 'false'));
        
        $mq_result = null;
        $rkey = null;
        
        foreach ($reminders as $key => $reminder) {
            
            $mq_result = $this->db->select()
                            ->from(array('mq' => 'mail_queue'))
                            ->join(array('p' => 'posts'), 'p.id = mq.post_id', array('reply_email', 'reply_email_real'))
                            ->where('mq.replied <> 1')
                            ->where('mq.remind = ?', $key)
                            ->where('mq.sent_date IS NOT NULL')
                            ->where('mq.sent_status = "success"');
            
            if (!$anonymous) {
                $mq_result->where(new Zend_Db_Expr('(p.reply_email not like "%craigslist.org%" OR mq.custom_email not like "%craigslist.org%")'));
            }
            
            if ($key == 0) {
                $mq_result->where('datediff(NOW(), mq.sent_date) >= ?', $reminder['days'])
                          ->where('mq.remind_date IS NULL');
            }
            else {
                $mq_result->where('datediff(NOW(), mq.remind_date) >= ?', $reminder['days'])
                          ->where('mq.remind_date IS NOT NULL');
            }
            
            $mq_result = $mq_result->order('mq.id DESC')->limit(1)->query()->fetch();
            
            // try next reminder days
            if (!$mq_result) {
                continue;
            }
            
            // if we found queue items that needs to remind then stop this cycle
            else {
                $rkey = $reminder['id'];
                break;
            }
        }
        
        $mq_result['rkey'] = $rkey;
        return $mq_result;
    }
    
    public function getQueueItem($anonymous=true)
    {
        $this->logger->logAction('getQueueItem', ($anonymous ? 'true' : 'false'));
        
        $mq_result = ($this->what == ITS_MAILER_TIME)
                        ? ($anonymous ? $this->getMailerItem() : $this->getNAMailerItem())
                        : $this->getReminderItem($anonymous);
        
        if (!$mq_result) {
            
            $this->logger->logAction('getQueueItem: not found any item');
            return null;
        }
        
        return $this->prepareItem($mq_result);
    }
    
    public function getLastSentLogRecord()
    {
        $this->logger->logAction('getLastSentLogRecord');
        return $this->db->select()->from('sent_log')->order('id DESC')->limit(1)->query()->fetch();
    }
    
    public function getLastSentPing()
    {
        $this->logger->logAction('getLastSentPing');
        return $this->db->select()->from('sent_log')->where('ping = ?', '1')->order('id DESC')->limit(1)->query()->fetch();
    }

    private function checkMailContent($qmail)
    {
        $error_message = null;
        
        if (!preg_match("/[A-Z0-9._%+-]+@[a-z0-9-]+\.[a-z]{2,4}\.?[a-z]{0,4}+/im", $qmail['reply_email'])) {
            $error_message = 'Was found invalid email address for queue item #'.$qmail['id'];
        }
        else if (!isset($qmail['subject']) || empty($qmail['subject'])) {
            $error_message = 'The subject is not set for queue item #'.$qmail['id'];
        }
        else if (!isset($qmail['body']) || empty($qmail['body'])) {
            $error_message = 'The body is not set for queue item #'.$qmail['id'];
        }
        
        $this->logger->logAction('checkMailContent', $error_message);
        
        if ($error_message) {
            $this->logger->log('Error! '.$error_message);
        }
        
        $this->logger->log('checkMailContent: success ');
        return $error_message ? false : true;
    }
    
    public function checkMailData($qmail)
    {
        $this->logger->log('Check mail data');
        
        if (!$this->checkMailContent($qmail)) {
            
            $this->markAsError($qmail);
            $this->logger->logAction('checkMailData', 'false');
            
            return false;
        }
        
        $this->logger->logAction('checkMailData', 'true');
        return true;
    }
    
    public function markAsError($qmail)
    {
        $this->logger->log('Try to update this queue item as error');
        $this->db->update('mail_queue', array(
                'sent_status' => 'error',
                (($this->what == ITS_MAILER_TIME) ? 'sent_date' : 'remind_date') => date('Y-m-d H:i:s')
            ),
            array('id = ?' => $qmail['id'])
        );
    }
    
    public function markAsSuccess($qmail)
    {
        $this->logger->logAction('markAsSuccess');
        
        $fieldsToUpdate = ($this->what == ITS_MAILER_TIME)
                ? array('sent_status' => 'success', 'sent_date' => date('Y-m-d H:i:s'))
                : array('remind_date' => date('Y-m-d H:i:s'), 'remind' => new Zend_Db_Expr('remind + 1'));
        
        $this->logger->log('Try to update this queue item as success');
        $this->db->update('mail_queue', $fieldsToUpdate, array('id = ?' => $qmail['id'])
        );
    }
}
