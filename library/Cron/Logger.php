<?php

class Cron_Logger {
    
    /**
     * Log file with common info
     * @var String
     */
    private $f_mailer;
    /**
     * Log file with info about cron mailer pings
     * @var String
     */
    private $f_mailer_test;
    /**
     * Log file with info about sending emails
     * @var String
     */
    private $f_mailer_send;
    
    /**
     * Log file with info about all action during mailer lifecycle
     * @var String
     */
    private $f_action;
    
    /**
     * Init
     */
    public function __construct()
    {
        $this->f_mailer = APPLICATION_PATH.'/../tmp/mailer.log';
        $this->f_mailer_test = APPLICATION_PATH.'/../tmp/mailer.test.log';
        $this->f_mailer_send = APPLICATION_PATH.'/../tmp/mailer.send.log';
        $this->f_action = APPLICATION_PATH.'/../tmp/action.log';
    }
    
    /**
     * Truncate log file if its needed and start logging
     */
    public function initLog()
    {
        $mode = (file_exists($this->f_mailer) && filesize($this->f_mailer) < 50000000) ? 'a+' : 'w+';
        $log = fopen($this->f_mailer, $mode);
        
        fwrite($log, "Begin log.\n\n");
        fclose($log);
    }
    
    /**
     * Add action data into log file
     * @param mixed $info
     * @param mixed $data
     */
    public function logAction($info, $data=null)
    {
        $log = fopen($this->f_action, 'a');
        
        fwrite($log, "\ntime: ".date('Y-m-d H:i:s'));
        fwrite($log, ": " . $info);
        
        if ($data)  {
            fwrite($log, "\n" . (is_array($data) ? implode(', ', $data) : $data));
        }
        
        fclose($log);
    }
    
    /**
     * Add some data into log file
     * @param mixed $data
     */
    public function log($data)
    {
        $log = fopen($this->f_mailer, 'a');
        fwrite($log, "\n\ntime: ".date('Y-m-d H:i:s'));
        fwrite($log, "\n" . (is_array($data) ? implode(', ', $data) : $data));
        fclose($log);
    }
    
    /**
     * Add info about cron mailer ping emails
     * @param String $to
     * @param String $from
     */
    public function logTest($to, $from)
    {
        $log = fopen($this->f_mailer_test, 'a+');
        fwrite($log, "\ntime: ".date('Y-m-d H:i:s')." | was sent test email to: {$to} from {$from}");
        fclose($log);
    }
    
    /**
     * Log that some email was sent from X to Y
     * @param String $to
     * @param String $from
     */
    public function logSendMail($to, $from, $host)
    {
        $log = fopen($this->f_mailer_send, 'a+');
        fwrite($log, "\ntime: ".date('Y-m-d H:i:s')." | was sent email to: {$to} from {$from} : {$host}");
        fclose($log);
    }
    
    /**
     * Log additional data into mailer send log file
     * @param String $data
     */
    public function logSendMailCommon($data)
    {
        $log = fopen($this->f_mailer_send, 'a+');
        fwrite($log, "\ntime: ".date('Y-m-d H:i:s').' | '.$data);
        fclose($log);
    }
}
