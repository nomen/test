<?php

class Cron_Mailer {
    
    private $db = null;
    private $logger = null;
    private $what = null;
    private $sentOrderId = null;
    
    public $smtpHosts = array();
    public $accounts = array();
    
    // 067 359 6406
    // 050 761 4899
    
    public function __construct($dbAdapter = null)
    {
        if ($dbAdapter) {
            $this->db = $dbAdapter;
        }
        else {
            
            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
            $this->db = new Zend_Db_Adapter_Mysqli($config->resources->db->params);
        }
        
        $tmp_acc = $this->db->select()->from('smtp_accounts')->query()->fetchAll();
        foreach ($tmp_acc as $acc) {
            $this->accounts[$acc['id']] = array(
                'id' => $acc['id'],
                'smtp_host_type' => $acc['host_type'],
                'user_email' => $acc['email'],
                'user_password' => $acc['password'],
                'user_name_from' => $acc['name_from']
            );
        }

        $tmp_hosts = $this->db->select()->from('smtp_hosts')->query()->fetchAll();
        foreach ($tmp_hosts as $host) {

            $this->smtpHosts[$host['id']] = array(
                'id' => $host['id'],
                'url' => $host['url'],
                'xmailer_id' => $host['xmailer_id'],
                'ehlo_id' => $host['ehlo_id'],
                'smtp_accounts_id' => $host['smtp_accounts_id']
            );
        }
    }
    
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
    
    public function setWhat($what)
    {
        $this->logger->logAction('setWhat', $what);
        $this->what = $what;
    }
    
    public function sendErrorMessage($qmail, $reason = null)
    {
        if (!$reason) {
            $reason = "1. не валидный email-адрес\n" . "2. не указана тема\n" . '3. нет тела письма';
        }
        
        $this->logger->logAction('sendErrorMessage', $reason);
        $this->logger->log('Send email with error info to developer');
        mail(
            'imaksymenko@webdevs.com', //llitvin@webdevs.com
            'Cron.Mailer.Error',
            "Инфо:\n"
            . 'Объявление в тулзе: http://craiglist.p.webdevs.com/index/showpost/?pid='.$qmail['post_id']."\n"
            . 'Объявление оригинал: '.$qmail['link']."\n\n"
            . "Возможные причины ошибки:\n"
            . $reason
        );
    }
    
    public function sendErrorToDeveloper($data)
    {
        $this->logger->logAction('sendErrorToDeveloper');
        $this->logger->log('Send email with error info to developer');
        mail(
            'imaksymenko@webdevs.com',
            'Cron.Mailer.Custom.Error',
            $data
        );
    }
    
    public function sendReportToDeveloper($data)
    {
        $this->logger->logAction('sendReportToDeveloper');
        $this->logger->log('Send email with report info to developer');
        mail(
            'imaksymenko@webdevs.com',
            'Cron.Mailer.Report',
            $data
        );
    }
    
    private function getPhoneNumber()
    {
        $phone = array('347 722 1951', '347-722-1951', '3477221951', '+13477221951', '+1 347 722 1951', '+1-347-722-1951');
        shuffle($phone);
        
        $this->logger->logAction('getPhoneNumber', $phone[0]);
        return $phone[0];
    }
    
    private function getSignature()
    {
        $this->logger->logAction('getSignature');
        $sign = '
            <p style="line-height:1.15; margin-top:0pt; margin-bottom:0pt; font-family:Calibri; font-size:12px;">
                <span>Irina Stepanenko | <a href="tel:347%20722%201951" value="+13477221951" target="_blank">'.$this->getPhoneNumber().'</a> |</span>
                <a href="http://www.linkedin.com/in/irynastepanenko" style="text-decoration:none" target="_blank">
                    <span style="text-decoration:underline;">www.linkedin.com/in/<wbr>irynastepanenko</span>
                </a>
                <span> |</span>
                <a href="http://www.webdevs.com/" style="text-decoration:none" target="_blank">
                    <span style="text-decoration:underline;">http://www.webdevs.com</span>
                </a>
            </p>';
        
        return $sign;
    }
    
    private function custom_shuffle($delimeter, $tomix, $mix, $tpl)
    {
        if (trim($delimeter) == ',' || trim($delimeter) == '|') {

            $mixed = explode($delimeter, $tomix);
            shuffle($mixed);
            $mixed = implode($delimeter, $mixed);

            return str_replace($mix[0][0], $mixed, $tpl);
        }
        else if (trim($delimeter) == 'NL') {

            $delimeter = "\n";
            $mixed = preg_split("/\\r\\n|\\r|\\n/", $mix[2][0]);

            foreach ($mixed as $key => $line) {
                if (!$line) {
                    unset($mixed[$key]);
                }
                else {

                    if (preg_match("/<submix\s*delimeter\s*=\s*\'(.*?)\'>(.*?)<\/submix>/ism", $mixed[$key], $sub_mix, PREG_OFFSET_CAPTURE)) {

                        $sub_delimeter = $sub_mix[1][0];
                        $sub_tomix = $sub_mix[2][0];

                        $mixed[$key] = $this->custom_shuffle($sub_delimeter, $sub_tomix, $sub_mix, $mixed[$key]);
                    }
                }
            }

            shuffle($mixed);
            $mixed = implode($delimeter, $mixed);

            return str_replace($mix[0][0], $mixed, $tpl);
        }
    }
    
    public function prepareMessageBody($qbody)
    {
        $this->logger->logAction('prepareMessageBody');
        
        $tpl = $qbody;
        $tpl .= "<br /><br />Kind regards,<br />=====================================<br />".
                "<MIX delimeter=' | '>" . "Irina Stepanenko" .
                    " | " . $this->getPhoneNumber() .
                    " | <a href='http://goo.gl/rAvWG'>LinkedIn</a>" .
                "</MIX>";
        
        $mix = null;
        do {
            
            if (!preg_match("/<mix\s*delimeter\s*=\s*\'(.*?)\'>(.*?)<\/mix>/ism", $tpl, $mix, PREG_OFFSET_CAPTURE)) {
                break;
            }

            $delimeter = $mix[1][0];
            $tomix = $mix[2][0];
            
            $tpl = $this->custom_shuffle($delimeter, $tomix, $mix, $tpl);
        }
        while(true);
        
        return $tpl;
    }
    
    private function getNextParams()
    {
        $this->logger->logAction('getNextParams');
//        return $this->db->select()->from('sent_order')->where('used = ?', '0')->limit(1)->query()->fetch();
        
        $sent_order = $this->db->select()
                        ->from(array('sr' => 'sent_order'), array('id', 'host_id'))
                        ->join(array('sh' => 'smtp_hosts'), 'sh.id = sr.host_id', array('sh.url', 'sh.xmailer_id', 'sh.ehlo_id'))
                        ->join(array('sa' => 'smtp_accounts'), 'sa.id = sh.smtp_accounts_id',
                               array('account_id' => 'sa.id', 'smtp_host_type' => 'sa.host_type', 'user_email' => 'sa.email',
                                     'user_password' => 'sa.password', 'user_name_from' => 'sa.name_from'))
                        ->join(array('sx' => 'smtp_xmailers'), 'sx.id = sh.xmailer_id', array('xmailer_name' => 'sx.name'))
                        ->join(array('se' => 'smtp_ehlo'), 'se.id = sh.ehlo_id', array('ehlo_name' => 'se.name'))
                        ->where('sr.used = ?', '0')
                        ->limit(1)->query()->fetch();
        
        return $sent_order;
    }
    
    private function customShuffle($what)
    {
        $this->logger->logAction('customShuffle');
        
        $replaceIndex = count($what)/2;
        $tmp = $what[0];
        $what[0] = $what[$replaceIndex];
        $what[$replaceIndex] = $tmp;
        
        return $what;
    }
    
    private function initSentOrder()
    {
        $this->logger->logAction('initSentOrder');
        
        // try to get last record info
        $last = $this->db->select()->from('sent_order')->order('id DESC')->limit(1)->query()->fetch();
        
        $smtpHosts = array_keys($this->smtpHosts);
        shuffle($smtpHosts);
        if ($last && $last['host_id'] == $smtpHosts[0]['id']) {
            $smtpHosts = $this->customShuffle($smtpHosts);
        }
        
        $accounts = array_keys($this->accounts);
        shuffle($accounts);
        if ($last && $last['account_id'] == $accounts[0]['id']) {
            $accounts = $this->customShuffle($accounts);
        }
        
        $iAccounts = 0;
        $iAccSize = count($this->accounts);
        $this->db->query('TRUNCATE craiglist.sent_order');
        
        foreach ($smtpHosts as $xHost) {
            
            $this->db->insert('sent_order', array(
                'host_id' => $xHost,
                'account_id' => $accounts[$iAccounts]
            ));
            
            $iAccounts = ($iAccounts >= $iAccSize-1) ? 0 : $iAccounts+1;
        }
    }
    
    private function getBccList($retTypeArray = false)
    {
        $tmpResult = $this->db->select()
                        ->from('users', array('email'))
                        ->where('bcc = ?', '1')
                        ->query()->fetchAll();
        
        if (!$tmpResult) {
            return null;
        }
        
        $result = array();
        foreach ($tmpResult as $tres) {
            $result[] = $tres['email'];
        }
        
        return ($retTypeArray) ? $result : implode(',', $result);
    }
    
    public function getSendParameters()
    {
        $this->logger->logAction('getSendParameters');
        
        $nextParams = $this->getNextParams();
        
        if (!$nextParams) {
            
            $this->initSentOrder();
            $nextParams = $this->getNextParams();
        }
        
        $this->sentOrderId = $nextParams['id'];
        return $nextParams;
    }
    
    public function doSendViaXSMTP($host, $emailTo, $subject, $msgBody, $qmail=null)
    {
        $this->logger->logAction('doSendViaXSMTP', array($emailTo, $host['user_email'], $host['url']));
        
        $client = new Zend_Http_Client($host['url']);
        $client->setParameterPost(array(
            'smtp_host' => $host['smtp_host_type'],
            'username' => $host['user_email'],
            'password' => $host['user_password'],
            'from_name' => $host['user_name_from'],
            'sendto' => $emailTo,
            'subject' => "RE: {$subject}",
            'msgbody' => $msgBody,
            'bcclist' => $this->getBccList(),
            'xmailer' => $host['xmailer_name'],
            'ehlo' => $host['ehlo_name']
        ));
        
        $this->logger->logSendMail($emailTo, $host['user_email'], $host['url']);
        
        $client->setConfig(array('strictredirects' => true));
        $client->setHeaders("Content-Type: text/html");
        $response = $client->request(Zend_Http_Client::POST); 
        
        $responseBody = $response->getBody();
        if (preg_match('/(failed|error|failure|array)/ism', $responseBody)) {
            
            if (preg_match('/(Password command failed|Please log in via your web browser)/ism', $responseBody)) {
                
                $message = '<span style="color: darkred;">Внимание!</span><br />';
                $message .= 'Рассылка и ремайндеры остановлены.<br />';
                
                if ($qmail) {
                    
                    $message .= 'Нужно вручную разблокировать аккаунт.<br /><br />';
                    $message .= 'Для этого нужно залогиниться в нужный аккаунт и ввести капчу.<br />';
                    $message .= 'Если капчи не было, нужно вручную подтвердить попытку входа, здесь:<br />';
                    $message .= 'https://security.google.com/settings/security/activity?pli=1';
                    
                    $message .= '<br /><br />Текст письма указанный ниже нужно отправить адресату вручную.<br />';
                    $message .= 'После этого не забудь включить рассылку!';
                }
                else {
                    $message .= '<span style="color: darkred;">Мила, ничего не делай! Я должен сам все проверить.</span>';
                }
                
                $message .= "<br /><br />Инфо:<br /><br />";
                if ($qmail) {
                    
                    $message .= 'Объявление в тулзе: http://craiglist.p.webdevs.com/index/showpost/?pid=' . $qmail['post_id'] . "<br />";
                    $message .= 'Объявление оригинал: ' . $qmail['link'] . "<br /><br />";
                }
                
                $message .= 'Ошибка возникла при попытке отправить письмо с аккаунта: ' . "<br />";
                $message .= 'email: ' . $host['user_email'] . "<br />";
                $message .= 'passw: ' . $host['user_password'] . "<br /><br />";
                
                $message .= 'Адресат: ' . $emailTo . "<br /><br />";
                
                if (!$qmail)  {
                    
                    $message .= 'Хост: ' . $host . "<br />";
                    $message .= 'Хост type: ' . $host['smtp_host_type'] . "<br /><br />";
                }
                
                $message .= 'Тема сообщения: <br />';
                $message .= "RE: {$subject}<br /><br />";
                
                $message .= 'Текст сообщения: <br />';
                $message .= $msgBody; /* preg_replace('/<br(\s*)?\/?>/i', "\n", $msgBody);*/
                
                $theme = 'Ошибка при '. (($qmail) ? 'рассылке' : 'пинге');
                
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                
                mail('imaksymenko@webdevs.com,llitvin@webdevs.com', $theme, $message, $headers);
            }
            
            // !!!!!!!!!!!!!!!! STOP THE QUEUE !!!!!!!!!!!!!!!!
            $this->db->update(
                'cron_job_setup',
                array('mailer_queue' => '0', 'reminder' => '0'),
                array('id = ?' => '1')
            );
            
            $this->logger->logAction('Mail Queue STOPPED!');
            
            $this->sendErrorToDeveloper($responseBody);
            return false;
        }
        
        $this->sendReportToDeveloper($responseBody);
        
        return true;
    }
    
    public function updateSentOrder()
    {
        $this->logger->logAction('updateSentOrder');
        
        $this->db->update(
            'sent_order',
            array('used' => '1'),
            array('id = ?' => $this->sentOrderId)
        );
    }
    
    private function updateSentLog($host_info, $template_id)
    {
        $this->logger->logAction('updateSentLog');
        
        $this->db->insert('sent_log', array(
            'host_id' => $host_info['host_id'],
            'account_id' => $host_info['account_id'],
            'template_id' => $template_id,
            'sent_date' => date('Y-m-d H:i:s')
        ));
    }
    
    public function sendAnonymousMail($qmail)
    {
        $this->logger->logAction('sendAnonymousMail');
        $sendres = true;
        
        // select server to send from
        $host = $this->getSendParameters();
        
        // prepare message body
        $msgBody = ($this->what == ITS_MAILER_TIME) ? $this->prepareMessageBody($qmail['body']) : $qmail['body'];
        
        if ($this->what == ITS_REMINDER_TIME) {
            
            // get template text for blockquote
            $tpl = $this->db->select()->from('templates')->where('id = ?', $qmail['template_id'])->query()->fetch();
            $tplBody = nl2br($this->prepareMessageBody($tpl['content']));
            
            $msgBody .= '
                <br />
                <br />
                <blockquote class="gmail_quote" style="margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex">
                    <div>' . $tplBody . '</div>
                </blockquote>
            ';
        }
        
// HARDCODED <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        $emailTo = $qmail['reply_email'];
//        $emailTo = 'imaksymenko@webdevs.com';
        
        // send
        try {
            $sendres = $this->doSendViaXSMTP($host, $emailTo, $qmail['subject'], $msgBody, $qmail);
        }
        catch (Exception $ex) {
            
            sleep(10);
            try {
                $sendres = $this->doSendViaXSMTP($host, $emailTo, $qmail['subject'], $msgBody, $qmail);
            }
            catch (Exception $ex) {
                
                $sendres = false;
                $this->logger->logSendMailCommon('ERROR !!!');
                $this->logger->log('!!!!! ERROR !!!!! While trying to send email to '.$qmail['email']);
                $this->logger->log("{$ex->getMessage()}\n\n{$ex->getFile()}\n\n{$ex->getTraceAsString()}");
                $this->logger->logAction('sendAnonymousMail: !!!!! ERROR !!!!! While trying to send email to '.$qmail['email']);
                $this->logger->logAction("sendAnonymousMail: {$ex->getMessage()}\n\n{$ex->getFile()}\n\n{$ex->getTraceAsString()}");
                
                print_r($ex->getMessage());
                print_r($ex->getCode());
                print_r($ex->getFile());
                print_r($ex->getTraceAsString());
            }
        }
        
        $this->updateSentOrder();
        $this->updateSentLog($host, $qmail['template_id']);
        
        return $sendres;
    }
    
    public function sendNonAnonymousMail($qmail)
    {
        $this->logger->logAction('sendNonAnonymousMail');
        
        $config = array(
            'auth' => 'login',
            'username' => 'llitvin@webdevs.com',
            'password' => 'SalesLeads02',
            'ssl' => 'tls'
        );
        
        $formatedContent = ($this->what == ITS_MAILER_TIME) ? $this->prepareMessageBody($qmail['body']) : $qmail['body'];
        
        if ($this->what == ITS_REMINDER_TIME) {
            
            // get template text for blockquote
            $tpl = $this->db->select()->from('templates')->where('id = ?', $qmail['template_id'])->query()->fetch();
            $tplBody = nl2br($this->prepareMessageBody($tpl['content']));
            
            $formatedContent .= '
                <br />
                <br />
                <blockquote class="gmail_quote" style="margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex">
                    <div>' . $tplBody . '</div>
                </blockquote>
            ';
        }
        
        $email = $qmail['reply_email'];
// HARDCODED <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//        $email = 'imaksymenko@webdevs.com';
        
        $tr = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
        Zend_Mail::setDefaultTransport($tr);
        $config['fake_from'] = 'istepanenko@webdevs.com';
        
        $oMail = new Zend_Mail('UTF-8');
        $oMail->addTo($email, '');
        
        $oMail->setSubject("RE: {$qmail['subject']}");
        $oMail->setBodyHtml($formatedContent);
        $oMail->setReturnPath($config['fake_from']);
        $oMail->setReplyTo($config['fake_from'], 'Irina Stepanenko');
        $oMail->setDefaultReplyTo($config['fake_from'], 'Irina Stepanenko');
        $oMail->setFrom($config['fake_from'], 'Irina Stepanenko');
        $oMail->setDefaultFrom($config['fake_from'], 'Irina Stepanenko');
        
        $bccList = $this->getBccList(true);
        foreach ($bccList as $bcc) {
            $oMail->addBcc($bcc);
        }
        
        $sendres = true;
        try {
            $sendres = $oMail->send();
        }
        catch (Exception $e) {
            
            sleep(10);
            try {
                $sendres = $oMail->send();
            }
            catch (Exception $e) {
                
                $sendres = false;
                $this->logger->logSendMailCommon("\ntime: ".date('Y-m-d H:i:s')." | ERROR !!!");
                $this->logger->log('!!!!! ERROR !!!!! While trying to send email to '.$qmail['email']);
                $this->logger->log("{$e->getMessage()}\n\n{$e->getFile()}\n\n{$e->getTraceAsString()}");
                $this->logger->logAction('sendNonAnonymousMail: !!!!! ERROR !!!!! While trying to send email to '.$qmail['email']);
                $this->logger->logAction("sendNonAnonymousMail: {$e->getMessage()}\n\n{$e->getFile()}\n\n{$e->getTraceAsString()}");
                
                print_r($e->getMessage());
                print_r($e->getCode());
                print_r($e->getFile());
                print_r($e->getTraceAsString());
            }
        }
        
        return $sendres;
    }
}
