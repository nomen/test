<?php

class App_Pinger {
    
    public static function getPingOrder()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->select()
                    ->from(array('spo' => 'sent_ping_order'), array('spo_id' => 'id', 'checkpoint_id', 'used'))
                    ->join(array('chp' => 'checkpoints'), 'chp.id = spo.checkpoint_id', array('id', 'reply_email', 'subject', 'link', 'account_login', 'account_pwd'))
                    ->joinLeft(array('sl' => 'sent_log'), 'sl.checkpoint_id = chp.id', array('host_id', 'account_id', 'template_id', 'sent_date'))
                    ->order('spo.id ASC')
                    ->group('chp.reply_email')
                    ->query()->fetchAll();
    }
    
    public static function getLastPingLog()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->select()
                    ->from('sent_log')
                    ->where('ping = ?', '1')
                    ->order('id DESC')
                    ->limit(1)
                    ->query()->fetch();
    }
    
    public static function getTplStat()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->select()
                    ->from(array('tpl' => 'templates'), array('tpl_id' => 'id', 'caption', 'content', 'modified', 'status'))
                    ->join(array('spt' => 'sent_ping_tpl'), 'spt.template_id = tpl.id', array('used_cnt', 'in_progress'))
                    ->order('tpl.id')
                    ->query()->fetchAll();
    }
    
    public static function getPingInfo()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->select()
                    ->from(array('pi' => 'ping_info'), array('pi_id' => 'pi.id', 'pi.template_id', 'pi.sent', 'pi.receive', 'pi.receive_check_cnt'))
                    ->join(array('chp' => 'checkpoints'), 'chp.id = pi.checkpoint_id', array('account_login', 'account_pwd'))
                    ->join(array('sh' => 'smtp_hosts'), 'sh.id = pi.smtp_hosts_id', array('url'))
                    ->join(array('sa' => 'smtp_accounts'), 'sa.id = pi.smtp_accounts_id', array('email', 'password'))
                    ->join(array('sxm' => 'smtp_xmailers'), 'sxm.id = pi.smtp_xmailers_id', array('xmailer_name' => 'name'))
                    ->join(array('se' => 'smtp_ehlo'), 'se.id = pi.smtp_ehlo_id', array('ehlo_name' => 'name'))
                    ->order('pi.id')
                    ->query()->fetchAll();
        
//                    ->from(array('sl' => 'sent_log'), array('template_id', 'sent_date'))
//                    ->join(array('chp' => 'checkpoints'), 'chp.id = sl.checkpoint_id', array('reply_email'))
//                    ->join(array('sh' => 'smtp_hosts'), 'sh.id = sl.host_id', array('url'))
//                    ->join(array('sa' => 'smtp_accounts'), 'sa.id = sl.account_id', array('email'))
//                    ->join(array('sxm' => 'smtp_xmailers'), 'sxm.id = sh.xmailer_id', array('name'))
//                    ->join(array('se' => 'smtp_ehlo'), 'se.id = sh.ehlo_id', array('name'))
//                    ->order('sl.id')
//                    ->query()->fetchAll();
        
//        select sl.template_id, chp.reply_email, sl.sent_date, sh.url, sa.email
//        from sent_log as sl
//                inner join checkpoints as chp on chp.id = sl.checkpoint_id
//                inner join smtp_hosts as sh on sh.id = sl.host_id
//                inner join smtp_accounts as sa on sa.id = sl.account_id
//                inner join smtp_xmailers as sxm on sxm.id = sh.xmailer_id
//                inner join smtp_ehlo as se on se.id = sh.ehlo_id
    }
}
