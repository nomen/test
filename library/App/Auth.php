<?php

class App_Auth {

    public static function hasIdentity()
    {
        $auth = Zend_Auth::getInstance();
        return $auth->hasIdentity();
    }

    public static function getIdentity()
    {
        $auth = Zend_Auth::getInstance();
        return $auth->getIdentity();
    }
    
    public static function authenticate($email)
    {
        $auth = Zend_Auth::getInstance();
        $adapter = new App_Auth_Adapter($email);
        
        return $auth->authenticate($adapter);
    }
    
    public static function clearIdentity()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
    }
}
