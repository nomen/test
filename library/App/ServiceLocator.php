<?php

/**
 * Object registry
 *
 * @author igor
 * 
 */
class App_ServiceLocator {

    protected static $storage = array();
    
    public static function gs()
    {
        return self::$storage;
    }
    
    public static function __callStatic($name, $args)
    {
        return self::load($name);
    }

    public static function store($name, $obj)
    {
        $parts = explode('::', $name, 2);
        
        if (count($parts) > 1) {
            $name = $parts[1];
        }
        
        self::$storage[$name] = $obj;
    }
    
    public static function isStored($name)
    {
        $parts = explode('::', $name, 2);
        
        if (count($parts) > 1) {
            $name = $parts[1];
        }
        
        return array_key_exists($name, self::$storage);
    }

    public static function load($name)
    {
        $parts = explode('::', $name, 2);
        
        if (count($parts) > 1) {
            $name = $parts[1];
        }
        
        if (array_key_exists($name, self::$storage)) {
            return self::$storage[$name];
        }
        else {
            throw new Exception('Nothing named key "'.$name.'" found by locator.');
        }
    }

    /**
     * @return Zend_Db_Adapter_Abstract
     */
    public static function DatabaseAdapter()
    {
        return self::load(__METHOD__);
    }

    /**
     * @return App_DateProvider
     */
    public static function DateProvider()
    {
        if (!array_key_exists(__METHOD__, self::$storage)) {
            self::store(__METHOD__, new App_DateProvider());
        }
        
        return self::load(__METHOD__);
    }

    /**
     * @return App_Model_Observer_Interface
     */
    public static function ModelObserver()
    {
        if (!array_key_exists(__METHOD__, self::$storage)) {
            self::store(__METHOD__, new App_Model_Observer());
        }
        
        return self::load(__METHOD__);
    }
}
