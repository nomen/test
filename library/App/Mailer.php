<?php

class App_Mailer {
    
    private static $qtable = 'mail_queue';
    private static $cronTable = 'cron_job_setup';
    
    public static function addMailToQueue($post_id, $template_id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->insert(self::$qtable, array(
            'post_id' => $post_id,
            'template_id' => $template_id,
            'custom' => '0'
        ));
    }
    
    public static function addCustomMailToQueue($post_id, $email, $subject, $body)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->insert(self::$qtable, array(
            'post_id' => $post_id,
            'custom_email' => $email,
            'custom_subject' => $subject,
            'custom_body' => $body,
            'custom' => '1'
        ));
    }
    
    public static function isMailerStarted()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = $db->select()->from(self::$cronTable, array('mailer_queue'))->query()->fetch();
        
        return (bool)$result['mailer_queue'];
    }
    
    public static function updateMailerStatus($newStatus)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->update(self::$cronTable, array('mailer_queue' => $newStatus));
    }
    
    public static function setEmailRepliedStatus($data)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->update(self::$qtable, array('replied' => 1, 'replied_as' => $data['replied_as'], 'replied_comment' => ($data['replied_comment']) ? $data['replied_comment'] : 'NULL'), array('id = ?' => $data['qid']));
    }
    
    public static function getQueueList()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $result = array();
        $qmail = $db->select()
                    ->from(array('mq' => self::$qtable), array('mq.id', 'mq.post_id', 'mq.template_id', 'mq.custom', 'mq.custom_email', 'mq.custom_subject'))
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array('p.link', 'p.reply_email', 'p.post_title', 'p.keywords'))
                    ->joinLeft(array('t' => 'templates'), 't.id = mq.template_id', array('t.caption'))
                    ->joinLeft(array('c' => 'city'), 'c.id = p.city_id', array('city_name' => 'c.name'))
                    ->where('mq.sent_status IS NULL')
                    ->query()->fetchAll();
        
        return $qmail;
    }
    
    public static function getSentEmailsList($for_statistics = false, $start_date = null, $end_date = null, $moderate_who = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $sent_date = ($for_statistics) ? 'DATE(mq.sent_date)' : 'mq.sent_date';
        $remind_date = ($for_statistics) ? 'DATE(mq.remind_date)' : 'mq.remind_date';
        
        $qmail = $db->select()
                    ->from(
                        array('mq' => self::$qtable),
                        array(
                            'mq.id',
                            'mq.post_id',
                            'mq.template_id',
                            'mq.custom',
                            'mq.custom_email',
                            'mq.custom_subject',
                            'mq.remind',
                            'sent_date' => $sent_date,
                            'remind_date' => $remind_date
                        )
                    )
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array('p.link', 'p.reply_email', 'p.post_title', 'p.keywords', 'p.post_date'))
                    ->joinLeft(array('t' => 'templates'), 't.id = mq.template_id', array('t.caption', 'tid' => 't.id'))
                    ->joinLeft(array('c' => 'city'), 'c.id = p.city_id', array('city_name' => 'c.name'))
                    ->joinLeft(array('s' => 'state'), 's.id = c.state_id', array('state_name' => 's.name'))
                    ->order('sent_date DESC');
        
        if (!$for_statistics) {
            
            $qmail->where('mq.replied = 0');
            $qmail->where('mq.sent_status IS NOT NULL');
        }
        else {
            $qmail->where('mq.sent_status = ?', 'success');
        }
        
        if ($start_date) {
            $qmail->where('DATE(sent_date) >= ?', $start_date);
        }

        if ($end_date) {
            $qmail->where('DATE(sent_date) <= ?', $end_date);
        }
        
        if (!$start_date && !$end_date) {
            $qmail->where('DATE(mq.sent_date) >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)');
        }
        
        if ($moderate_who) {
            $qmail->where("p.moderate_who LIKE '%{$moderate_who}%'");
        }
                    
        $result = $qmail->query()->fetchAll();
        return $result;
    }
    
    /**
     * 
     * @param int $iDisplayStart
     * @param int $iDisplayLength
     * @param int $iSortCol
     * @param int $sSortDir
     * @return mixed
     */
    public static function getSentEmailsListForReplied($iDisplayStart, $iDisplayLength, $iSortCol, $sSortDir)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $qmail = $db->select()
                    ->from(
                        array('mq' => self::$qtable),
                        array(
                            'mq.id',
                            'mq.post_id',
                            'mq.template_id',
                            'mq.custom',
                            'mq.custom_email',
                            'mq.custom_subject',
                            'mq.remind',
                            'mq.sent_date',
                            'mq.remind_date'
                        )
                    )
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array('p.link', 'p.reply_email', 'p.post_title', 'p.keywords', 'p.post_date'))
                    ->joinLeft(array('t' => 'templates'), 't.id = mq.template_id', array('t.caption', 'tid' => 't.id'))
                    ->joinLeft(array('c' => 'city'), 'c.id = p.city_id', array('city_name' => 'c.name'))
                    ->joinLeft(array('s' => 'state'), 's.id = c.state_id', array('state_name' => 's.name'))
                    ->where('mq.replied = 0')
                    ->where('mq.sent_status IS NOT NULL')
                    ->where('DATE(mq.sent_date) >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)')
                    ->limit($iDisplayLength, $iDisplayStart);
        
        if ($iSortCol != 'id') {
            $qmail->order('mq.remind desc');
        }
        
        $qmail->order(array('mq.sent_date DESC', 'mq.id '.$sSortDir));
        
//        echo $qmail->__toString();
        
        $result = $qmail->query()->fetchAll();
        return $result;
    }
    
    public static function searchEmails($s_id, $s_email, $s_state, $s_post_date, $s_sent_date, $s_title, $s_keyword, $s_template, $iSortCol, $sSortDir)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $qmail = $db->select()
                    ->from(
                        array('mq' => self::$qtable),
                        array(
                            'mq.id',
                            'mq.post_id',
                            'mq.template_id',
                            'mq.custom',
                            'mq.custom_email',
                            'mq.custom_subject',
                            'mq.remind',
                            'mq.sent_date',
                            'mq.remind_date'
                        )
                    )
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array('p.link', 'p.reply_email', 'p.post_title', 'p.keywords', 'p.post_date'))
                    ->joinLeft(array('t' => 'templates'), 't.id = mq.template_id', array('t.caption', 'tid' => 't.id'))
                    ->joinLeft(array('c' => 'city'), 'c.id = p.city_id', array('city_name' => 'c.name'))
                    ->joinLeft(array('s' => 'state'), 's.id = c.state_id', array('state_name' => 's.name'))
                    ->where('mq.replied = 0')
                    ->where('mq.sent_status IS NOT NULL')
//                    ->where('DATE(mq.sent_date) >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)')
                    ->order(array('mq.sent_date DESC', 'mq.id '.$sSortDir));
        
        if ($s_id)          {$qmail->where('mq.id LIKE ?', "%{$s_id}%");}
        if ($s_email)       {$qmail->where('p.reply_email LIKE ?', "%{$s_email}%");}
        if ($s_state)       {$qmail->where('s.name LIKE ?', "%{$s_state}%");}
        if ($s_title)       {$qmail->where('p.post_title LIKE ?', "%{$s_title}%");}
        if ($s_keyword)     {$qmail->where('p.keywords LIKE ?', "%{$s_keyword}%");}
        if ($s_template)    {$qmail->where('t.caption LIKE ?', "%{$s_template}%");}
        
        if ($s_post_date) {
            
            $s_post_date = date('Y-m-d', strtotime($s_post_date));
            $qmail->where('p.post_date LIKE ?', "%{$s_post_date}%");
        }
        
        if ($s_sent_date) {
            
            $s_sent_date = date('Y-m-d', strtotime($s_sent_date));
            $qmail->where('mq.sent_date LIKE ?', "%{$s_sent_date}%");
        }
        
        $result = $qmail->query()->fetchAll();
        return $result;
    }

    public static function getSentEmailsListCount()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $qmail = $db->select()
                    ->from(
                        array('mq' => self::$qtable),
                        array('cnt' => 'COUNT(1)')
                    )
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array())
                    ->joinLeft(array('t' => 'templates'), 't.id = mq.template_id', array())
                    ->joinLeft(array('c' => 'city'), 'c.id = p.city_id', array())
                    ->joinLeft(array('s' => 'state'), 's.id = c.state_id', array())
                    ->where('mq.replied = 0')
                    ->where('mq.sent_status IS NOT NULL')
                    ->where('DATE(mq.sent_date) >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)')
                    ->order('sent_date DESC');

        $result = $qmail->query()->fetch();
        return $result['cnt'];
    }

    public static function getQueueCount()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->select()->from(self::$qtable, array('COUNT(1) as cnt'))->where('sent_status IS NULL')->query()->fetch();
    }
    
    public static function getQueueCountSent()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->select()->from(self::$qtable, array('COUNT(1) as cnt'))->where('sent_status IS NOT NULL')->query()->fetch();
    }
    
    public static function alreadyQueued($post)
    {
        if (!isset($post['post_title']) || empty($post['post_title']) || !isset($post['reply_email']) || empty($post['reply_email'])) {
            return null;
        }
        
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $qmail = $db->select()
                    ->from(
                        array('mq' => self::$qtable),
                        array('mq.id', 'mq.post_id', 'sent_date')
                    )
                    ->joinLeft(array('p' => 'posts'), 'p.id = mq.post_id', array())
                    ->where('p.post_title = ?', $post['post_title'])
                    ->where('p.reply_email = ?', $post['reply_email']);
                    
        $result = $qmail->query()->fetchAll();
        
        return count($result);
    }
    
    public static function getCheckpointItems()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->select()->from('checkpoints')->query()->fetchAll();
    }
    
    public static function checkRenewTime()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $d1 = new Zend_Date();
        $items = $db->select()->from('checkpoints', array('renewed'))->query()->fetchAll();
        
        foreach ($items as $item) {
            
            $d2 = new Zend_Date(strtotime($item['renewed']));
            $diff = $d1->toValue() - $d2->toValue();
            $days = floor($diff/60/60/24);
            
            if ($days > 7) {
                return true;
            }
        }
        
        return false;
    }
}
