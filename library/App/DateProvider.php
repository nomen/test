<?php

/**
 * Provides date-related data
 *
 * @author igor
 */
class App_DateProvider
{
    const FORMAT_MYSQL = 'YYYY-MM-dd HH:mm:ss';
    
    /**
     * Return current date
     * @return Zend_Date
     */
    public function getDate()
    {
        $date = Zend_Date::now();
        return $date;
    }
    
    public function getCurrentDayType()
    {
        $type = $this->getDate()->toString(Zend_Date::WEEKDAY_DIGIT)==5?'m':'r';
        return $type;
    }
    
    public function getDateMysql($date)
    {
        if (!$date) {
            $date = $this->getDate();
        }
        
        return $date->toString(self::FORMAT_MYSQL);
    }
    
    /**
     *
     * @return int
     */
    public function getCurrentWeekdayNum()
    {
        return $this->getDate()->getWeekday()->toString(Zend_Date::WEEKDAY_8601);
    }
    
    /**
     *
     * @return string
     */
    public function getCurrentWeekday()
    {
        return $this->getDate()->toString(Zend_Date::WEEKDAY_SHORT);
    }
    
    /**
     * @return mixed
     */
    public function getWeekDays()
    {
        $result = array();
        $date = $this->getDate();
        
        for ($i = 1; $i < 6; $i++) {
            $result[$i] = $date->setWeekday($i)->toString(Zend_Date::WEEKDAY_SHORT);
        }
        
        return $result;
    }
	
    /**
     * @return mixed
     */
    public function getRegularDays()
    {
        $result = array();
        $date = $this->getDate();
        
        for ($i = 1; $i < 5; $i++) {
            $result[$i] = $date->setWeekday($i)->toString(Zend_Date::WEEKDAY_SHORT);
        }
        
        return $result;
    }
    
    public function getCurrentWeek()
    {
        return $this->getDate()->toString(Zend_Date::WEEK);
    }
}
