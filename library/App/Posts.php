<?php

class App_Posts {
    
    public static function searchPosts($status, $s_id = null, $s_state = null, $s_city = null, $s_date = null, $s_title = null, $s_keyword = null, $category = null, $accordingToModerateStatus = true, $sortCol=null, $sortDir=null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $result = $db->select()
                    ->from(array('p' => 'posts'), array('p.id', 'p.sessions_id', 'p.city_id', 'p.parsing_status', 'p.parsing_status_reason', 'p.keywords', 'p.reply_email', 'p.link', 'p.post_date', 'p.post_title', 'p.moderate_status', 'p.moderate_who', 'p.moderate_date'))
                    ->join(array('c' => 'city'), 'c.id = p.city_id', array('city_name' => 'c.name'))
                    ->join(array('s' => 'state'), 's.id = c.state_id', array('state_name' => 's.name'))
                    ->where('1 = 1')
                    ->where('parsing_status = ?', $status);
        
        if ($sortCol && $sortDir) {
            $result->order("post_date {$sortDir}");
        }
        else {
            $result->order('post_date DESC');
        }
        
        if ($s_id) {
            $result->where("p.id = ?", $s_id);
        }
        
        if ($s_state) {
            $result->where("s.name LIKE('%{$s_state}%')");
        }
        
        if ($s_city) {
            $result->where("c.name LIKE('%{$s_city}%')");
        }
        
        if ($s_date) {
            $result->where("p.post_date = ?", date('Y-m-d', strtotime($s_date)));
        }
        
        if ($s_title) {
            $result->where("p.post_title LIKE('%{$s_title}%')");
        }
        
        if ($s_keyword) {
            $result->where("p.keywords LIKE('%{$s_keyword}%')");
        }
        
        if ($category) {
            $result->where('p.category_id = ?', $category);
        }
        
        if ($accordingToModerateStatus) {
            $result->where('p.moderate_status IS NULL');
        }
        
        return $result->query()->fetchAll();
    }
    
    /**
     * 
     * @param string $status POST_STATUS_ACCEPTED or POST_STATUS_SKIPPED
     * @param bool $accordingToModerateStatus TRUE by default
     * @param int $start 
     * @param int $length
     * @param int $category
     * @return mixed
     */
//    public static function geLastPosts($status, $accordingToModerateStatus = true, $page = 1)
    public static function geLastPosts($status, $accordingToModerateStatus = true, $start = 0, $length = 100, $category = null, $real = null, $sortCol=null, $sortDir=null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $sql = "
            SELECT *
            FROM sessions
            ORDER BY id DESC
            LIMIT 1
        ";
        
        $sess_exist = $db->fetchRow($sql);

        if (!$sess_exist) {
            return $result;
        }
        
        $result = self::getPostsByStatus($status, false, null, $accordingToModerateStatus, $start, $length, $category, $real, $sortCol, $sortDir);
        return $result;
    }
    
    /**
     * 
     * @param type $status - Select posts with defined 'parsing_status' value (POST_STATUS_ACCEPTED or POST_STATUS_SKIPPED)
     * @param type $calcCount - Calculate count or return the list of data. FALSE by default
     * @param type $moderateStatus - Select posts with defined 'moderate_status' value (POST_MODERATE_STATUS_SENT or POST_MODERATE_STATUS_FAILED)
     * @param type $accordingToModerateStatus - 
     * @param type $start
     * @param type $length
     * @param type $category
     * @return type
     */
    public static function getPostsByStatus($status, $calcCount = false, $moderateStatus = null, $accordingToModerateStatus = true, $start = 0, $length = 100, $category = null, $real = null, $sortCol=null, $sortDir=null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $selectCond = $calcCount ? 'COUNT(1) AS cnt' : 'p.id, p.sessions_id, p.city_id, p.parsing_status, p.parsing_status_reason, p.keywords, p.link, p.reply_email, p.post_date, p.post_title, p.moderate_status, p.moderate_who, p.moderate_date, c.name AS city_name, st.name as state_name';
        $moderateStatus = $moderateStatus ? " AND moderate_status = '{$moderateStatus}'" : '';
        $accordingToModerateStatus = $accordingToModerateStatus ? 'AND (moderate_status="" OR moderate_status IS NULL)' : '';
        $category = $category ? "AND category_id = {$category}" : '';
        $limits = ($calcCount || !$accordingToModerateStatus) ? '' : 'LIMIT '.$start.', '.$length;
        $limits = ($length == -1) ? '' : $limits;
        
        $order = 'reply_email_real DESC';
        if ($sortCol && $sortDir) {
            $order .= ", post_date {$sortDir}";
        }
        else {
            $order .= ', post_date DESC';
        }
        
        $realFilter = '';
        if (isset($real) && $real == 1) {
        	$realFilter = "AND reply_email not like '%craigslist.org%' and reply_email like '%@%'";
        }
        
        $sql = "
            SELECT {$selectCond}
            FROM posts AS p
                INNER JOIN sessions AS s ON s.id = p.sessions_id
                INNER JOIN city as c on c.id = p.city_id
                INNER JOIN state as st on st.id = c.state_id
            WHERE   (parsing_status = '{$status}' OR parsing_status = 'raw')
                {$moderateStatus}
                {$accordingToModerateStatus}
                {$category}
                /*AND DATE(s.start) > DATE(NOW() - INTERVAL 1 MONTH)*/
                {$realFilter}
            ORDER BY {$order}
            {$limits}
        ";
        
        if (!$calcCount) {
            $result = $db->fetchAll($sql);
        }
        else {
            $result = $db->fetchRow($sql);
            $result = $result['cnt'];
        }
        
        return $result;
    }
    
    /**
     * 
     * @param type $status - Select posts with defined 'moderate_status' value (POST_MODERATE_STATUS_SENT or POST_MODERATE_STATUS_FAILED)
     * @param type $state
     * @param type $date
     * @param type $calcCount - Calculate count or return the list of data. FALSE by default
     * @return type
     */
    public static function getPostsByStatusStateDate($status, $state, $date, $calcCount = false)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $result = $db->select()
                    ->from(array('p' => 'posts'), array('p.id', 'p.city_id', 'p.post_date', 'p.moderate_status', 'p.moderate_date'))
                    ->join(array('c' => 'city'), 'c.id = p.city_id', array())
                    ->join(array('s' => 'state'), 's.id = c.state_id', array())
                    ->where('p.moderate_status = ?', $status)
                    ->where('DATE(p.moderate_date) = ?', $date)
                    ->where('s.name = ?', $state)
                    ->query()->fetchAll();
        
        return ($calcCount) ? count($result) : $result;
    }

    public static function getPostById($pid)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
//        $pid = mysql_escape_string($pid);
        
        if (!$pid) {
            return null;
        }
        
        return $db->fetchRow("SELECT * FROM posts WHERE id = {$pid}");
    }
    
    public static function geMismatchPosts($status)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $moderateStatus = ($status == POST_STATUS_ACCEPTED) ? POST_MODERATE_STATUS_FAILED : POST_MODERATE_STATUS_SENT;
        
        $sql = "
            SELECT p.id, p.sessions_id, p.city_id, p.parsing_status, p.parsing_status_reason, p.link, p.post_date, p.post_title, p.moderate_status, p.moderate_who, p.moderate_date
            FROM posts AS p
                INNER JOIN sessions AS s ON s.id = p.sessions_id
            WHERE   parsing_status = '{$status}'
                AND moderate_status = '{$moderateStatus}'
        ";
        
        $result = $db->fetchAll($sql);
        return $result;
    }
    
    public static function fixPostsLock()
    {
        $usr_info = (App_User::isLogged()) ? Zend_Auth::getInstance()->getIdentity() : null;
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->query("
            UPDATE posts
            SET moderate_lock = '0',
                moderate_who = NULL
            WHERE   moderate_lock = '1' 
                AND (moderate_date IS NULL OR moderate_date = '') 
                AND (moderate_status IS NULL OR moderate_status = '')
                AND moderate_who LIKE '%{$usr_info['email']}%'
        ");
    }
    
    public static function hasRealEmail($pid)
    {
        $pdata = self::getPostById($pid);
        if (preg_match('/craig[s]*list\.org/ism', $pdata['reply_email'])) {
            return false;
        }
        
        return true;
    }
    
    public static function hasReplyEmail($pid)
    {
        $result = self::getPostById($pid);
        return !empty($result['reply_email']);
    }
    
    public static function getReplyEmail($pid)
    {
        $result = self::getPostById($pid);
        return $result['reply_email'];
    }
    
    public static function prepareKeywords($keywords)
    {
        if (!isset($keywords) || empty($keywords)) {
            return null;
        }
        
        $result = "";
        $tmp_result = unserialize($keywords);
        
        if (empty($tmp_result) || !is_array($tmp_result)) {
            return null;
        }
        
        foreach ($tmp_result as $key) {
            $result .= "<span class='label'>{$key}</span> ";
        }
        
        return $result;
    }
    
    public static function removeInvalidChars($text)
    {
        $regex = '/( [\x00-\x7F] | [\xC0-\xDF][\x80-\xBF] | [\xE0-\xEF][\x80-\xBF]{2} | [\xF0-\xF7][\x80-\xBF]{3} ) | ./x';
        return preg_replace($regex, '$1', $text);
    }
    
    public static function storeTestedPosts($posts)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->delete('tested_posts', array('id > 0'));
        
        foreach ($posts as $p) {
            $db->insert('tested_posts', array('post_id' => $p));
        }
    }
    
    public static function getTestedPostsIds()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $result = $db->select()->from('tested_posts', array('post_id'))->query()->fetchAll();
        
        $str_result = '';
        foreach ($result as $res) {
            $str_result .= $res['post_id'].' ';
        }
        
        return trim($str_result);
    }
    
    public static function getTestedPostsData()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $result = array();
        
        // get all tested posts
        $tested_posts = $db ->select()
                            ->from(array('tp' => 'tested_posts'))
                            ->join(array('p' => 'posts'), 'p.id = tp.post_id', array('post_content', 'post_title', 'link'))
                            ->query()->fetchAll();
        
        // get all additional filter sets
        $sets = $db ->select()
                    ->from(array('s' => 'set'))
                    ->join(array('t' => 'templates'), 't.id = s.templates_id', array('priority', 'caption'))
                    ->order('priority ASC')
                    ->query()->fetchAll();
        
//        echo '<pre>';
//        print_r($tested_posts);
//        print_r($sets); die;
        
        foreach ($tested_posts as $tp) {
            
            $template = array();
            
            // check that custom post is match to the some of filter set
            foreach ($sets as $set) {

                // by exclude params
                $prepared_exclude = explode("|", $set['exclude']);
                $prepared_exclude = App_FormData::prepareMultiIncludeExcludeParams($prepared_exclude);

                if (App_FormData::checkForParams($prepared_exclude, $tp['post_content'])) {
                    continue;
                }

                // by include params
                $prepared_include = explode("|", $set['include']);
                $prepared_include = App_FormData::prepareMultiIncludeExcludeParams($prepared_include);

                if (!App_FormData::checkForParams($prepared_include, $tp['post_content'])) {
                    continue;
                }

                $template = array(
                    'id' => $set['templates_id'],
                    'name' => $set['caption']
                );
                
                break;
            }
            
            $colorize = 0;
            if ($template['id'] != $tp['template_id']) {
                
                $db->update('tested_posts', array('template_id' => $template['id']), array('id = ?' => $tp['id']));
                $colorize = ($tp['template_id'] == 0) ? 0 : 1;
            }
            
            // prepare result
            $result[] = array(
                'post_id' => $tp['post_id'],
                'post_title' => $tp['post_title'],
                'post_link' => $tp['link'],
                'template_id' => $template['id'],
                'template_name' => $template['name'],
                'colorize' => $colorize
            );
        }
        
//        print_r($result); die;
        
        return $result;
    }
}
