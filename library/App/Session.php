<?php

class App_Session {
    
    public static function sessionExists()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->select()->from('sessions')->order('id DESC')->limit(1)->query()->fetch();
    }
    
    public static function initSession($params)
    {
        if (!isset($params) || empty($params) || count($params) <= 3) {
            return false;
        }
        
        $db = Zend_Db_Table::getDefaultAdapter();
        
//        $params['include'] = preg_replace('/[\r\n]+/', '|', $params['include']);
//        $params['exclude'] = preg_replace('/[\r\n]+/', '|', $params['exclude']);
        
        // prepare additional params
        $telecommuting = (isset($params['add'][0])) ? 'telecommuting' : '';
        $contract = (isset($params['add'][1])) ? 'contract' : '';
        $internship = (isset($params['add'][2])) ? 'internship' : '';
        $part_time = (isset($params['add'][3])) ? 'part-time' : '';
        $non_profit = (isset($params['add'][4])) ? 'non-profit' : '';
        $hasPic = (isset($params['add'][5])) ? '1' : '';
        
        $srchType = (isset($params['srchType'])) ? $params['srchType'] : 'A';
        $since = (!empty($params['since'])) ? $params['since'] : "NULL";
        
        $params['places'] = (!isset($params['places'])) ? App_FormData::getPlaces() : $params['places'];
        
        $db->insert('sessions', array(
            'start' => date('Y-m-d H:i:s'),
            'status' => '0',
            'query' => $params['query'],
            'search_type' => $srchType,
            'category' => serialize($params['catAbb']),
            'advanced_add' => isset($params['add']) ? $params['add'] : NULL,
            'places' => serialize($params['places']),
//            'include' => $params['include'],
            'exclude' => $params['exclude'],
            'exclude_emails' => $params['exclude_emails'],
            'since' => $since
        ));
        
        $sessions_id = $db->lastInsertId();
        
        // store advanced session settings
        foreach ($params['places'] as $regions) {
            foreach ($regions as $state) {
                foreach ($state as $city) {
                    
                    $link = $db->select()->from('city', array('link'))->where('id = ?', $city)->query()->fetch();
                    $db->insert(
                        'sessions_advanced',
                        array(
                            'sessions_id' => $sessions_id,
                            'city_id' => $city,
                            'link' => $link['link'],
                        )
                    );
                }
            }
        }
        
        return $sessions_id;
    }
    
    public static function stopParserSession()
    {
        $sess_exist = self::sessionExists();
        $alreadyStarted = ($sess_exist && $sess_exist['status'] == 0) ? true : false;
        
        if (!$alreadyStarted) {
            return false;
        }
        
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->update(
            'sessions',
            array(
                'status' => '3',
                'end' => date('Y-m-d H:i:s')
            ),
            array('id = ?' => $sess_exist['id'])
        );
    }
}
