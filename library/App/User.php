<?php

require_once 'Auth/Adapter.php';

class App_User {

    /**
     * @var models_Users
     */
    protected static $loggedUser = null;

    /**
     * Checks if user is logged in
     * @return bool
     */
    public static function isLogged()
    {
        if (App_Auth::hasIdentity()) {
            
            $aIdentity = App_Auth::getIdentity();

            if (!empty($aIdentity)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
    public static function doOauthLogin($email)
    {
        $storage = new Auth_OpenID_FileStore($_SERVER['DOCUMENT_ROOT'] . '/../tmp/openid');
        
        $auth = Zend_Auth::getInstance();
        $result = App_Auth::authenticate($email);
        
        if ($result->isValid()) {
            
            $model = new Model_Users();
            $user = $model->findByEmail($email);

            if (!$user) {
                return false;
            }
            
            Zend_Session::rememberMe();

            $model->findUser($user['id']);
            $auth->getStorage()->write($user);
            
            return true;
        }
        
        return false;
    }
}
