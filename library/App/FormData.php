<?php

class App_FormData {
    
    public static function getJobsList()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        $tmpResult = $db->select()->from('category')->query()->fetchAll();
        
        foreach ($tmpResult as $res) {
            $result[$res['value']] = $res['name'];
        }
        
        return $result;
    }
    
    public static function getLastParams($params = null)
    {
        $result = App_Session::sessionExists();
        
        if ($result) {
            
            $result['srchType'] = $result['search_type'];
            $result['catAbb'] = $result['category'];
            $result['add'] = unserialize($result['advanced_add']);

            $result['include'] = preg_replace('/\|/', "\r\n", $result['include']);
            $result['exclude'] = preg_replace('/\|/', "\r\n", $result['exclude']);
        }
        else if ($params) {
            $result = $params;
        }
        else {
            $result = array(
                'query',
                'srchType',
                'catAbb',
                'category',
                'add',
                'include',
                'exclude',
                'since'
            );
        }
        
        return $result;
    }
    
    public static function getPlaces($test = false)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = array();
        
        $tmpResult = $db->select()
                        ->from(array('c' => 'city'), array('city_id' => 'c.id', 'city_name' => 'c.name', 'c.link'))
                        ->join(array('s' => 'state'), 's.id = c.state_id', array('state_id' => 's.id', 'state_name' => 's.name'))
                        ->join(array('r' => 'region'), 'r.id = s.region_id', array('region_id' => 'r.id', 'region_name' => 'r.name'))
                        ->where('r.name in("US","Canada","Europe","Oceania")');
        
        if ($test) {
            $tmpResult->distinct()->join(array('p' => 'posts'), 'p.city_id = c.id');
        }
        
        $lastSession = App_Session::sessionExists();
        $places = unserialize($lastSession['places']);
        
        $tmpResult = $tmpResult->query()->fetchAll();
        foreach ($tmpResult as $res) {
            
            $result[$res['region_id']]['region_name'] = $res['region_name'];
            $result[$res['region_id']][$res['state_id']]['state_name'] = $res['state_name'];
            
            if (isset($places[$res['region_id']][$res['state_id']])) {
                $result[$res['region_id']][$res['state_id']]['state_selected'] = 1;
            }
            
            $result[$res['region_id']][$res['state_id']][$res['city_id']] = array(
                'city_id' => $res['city_id'],
                'city_name' => $res['city_name'],
                'link' => $res['link']
            );
            
            if (isset($places[$res['region_id']][$res['state_id']][$res['city_id']])) {
                $result[$res['region_id']][$res['state_id']][$res['city_id']]['city_selected'] = 1;
            }
        }
        
        return $result;
    }
    
    public static function getSet()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $result = $db->select()->from('set')->query()->fetchAll();
        
        foreach ($result as $key=>$res) {
            
            $result[$key]['include'] = preg_replace('/\|/', "\r\n", $res['include']);
            $result[$key]['exclude'] = preg_replace('/\|/', "\r\n", $res['exclude']);
        }
        
        return $result;
    }
    
    public static function getTemplates()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->select()->from('templates', array('id', 'caption'))->order('priority ASC')->query()->fetchAll();
    }
    
    public static function getTemplateData($tid)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        if (!$tid) {
            return null;
        }
        
        return $db->select()->from('templates', array('id', 'caption', 'content'))->where('id = ?', $tid)->query()->fetch();
    }
    
    public static function getCustomTemplates($pid)
    {
        // get all existings templates
        $db = Zend_Db_Table::getDefaultAdapter();
        $templates = $db->select()
                        ->from('templates', array('id', 'caption'))
                        ->order('priority ASC');
        
        if (!App_Posts::hasRealEmail($pid)) {
            $templates->where('status = ?', 'clean');
        }
        
        $templates = $templates->query()->fetchAll();
        
        // check that this post has reply email
        if (!App_Posts::hasReplyEmail($pid)) {
            
            $templates['hasreply'] = 0;
            return $templates;
        }
        
        $template_id = null;
        
        // get custom post data
        $post = $db->select()->from('posts')->where("id = {$pid}")->query()->fetch();
        
        // get all additional filter sets
        $sets = $db ->select()
                    ->from(array('s' => 'set'))
                    ->join(array('t' => 'templates'), 't.id = s.templates_id', array('priority'))
                    ->order('priority ASC');
        
        if (!App_Posts::hasRealEmail($pid)) {
            $sets->where('t.status = ?', 'clean');
        }
        
        $sets = $sets->query()->fetchAll();
        
//        print_r($sets); die;
        
        // check that custom post is match to the some of filter set
        foreach ($sets as $set) {
            
            // by exclude params
//            $prepared_exclude = str_replace(' ', '\s*', $set['exclude']);
//            $prepared_exclude = str_replace('.', '\.', $set['exclude']);
            
            $prepared_exclude = explode("|", $set['exclude']);
            $prepared_exclude = self::prepareMultiIncludeExcludeParams($prepared_exclude);

//            if (preg_match("/\b({$prepared_exclude})\b/ism", $post['post_content'])) {
            if (self::checkForParams($prepared_exclude, $post['post_content'])) {
                continue;
            }

            // by include params
//            $prepared_include = str_replace(' ', '\s*', $set['include']);
//            $prepared_include = str_replace('.', '\.', $set['include']);
            
            $prepared_include = explode("|", $set['include']);
            $prepared_include = self::prepareMultiIncludeExcludeParams($prepared_include);

//            if (!preg_match("/\b({$prepared_include})\b/ism", $post['post_content'])) {
            if (!self::checkForParams($prepared_include, $post['post_content'])) {
                continue;
            }
            
            $template_id = $set['templates_id'];
            break;
        }
        
        // update templates data
        foreach ($templates as $key=>$tpl) {
            $templates[$key]['selected'] = ($templates[$key]['id'] == $template_id) ? 'selected="selected"' : '';
        }
        
        $templates['hasreply'] = 1;
        
        return $templates;
    }
    
    /**
    *  Two lines of exclusive filters are divided into 2 parts
    *  Array
       (
           [0] => php "looking for" .html "awd & zxc"
           [1] => "made Zappos.com" xxx
       )
    * 
    * Then from each row we separate parameters enclosed in quotes
    * and all parameters are collected in the sub-arrays of arrays of strings
    * 
       Array
       (
           [0] => Array
               (
                   [0] => looking for
                   [1] => awd & zxc
                   [2] => php
                   [3] => .html
               )

           [1] => Array
               (
                   [0] => made Zappos.com
                   [1] => xxx
               )

       )
    */
    public static function prepareMultiIncludeExcludeParams($params)
    {
        $prepared_excludes = array();
                        
        foreach ($params as $key => $exclude) {
            
            if (empty($exclude)) {
                continue;
            }

            // try to find quoted phrases
            $matches = null;
            preg_match_all("/\"(.*?)\"|\'(.*?)\'/ism", $exclude, $matches);

            $prepared_excludes[$key] = $matches[1];

            $rest = preg_replace('/\"(.*?)\"|\'(.*?)\'/ism', '', $exclude);
            $rest = trim($rest);
            $rest = preg_replace('/\s+/ism', ' ', $rest);
            $rest = explode(' ', $rest);
            
            if (isset($rest[0]) && empty($rest[0])) {
                continue;
            }

            $prepared_excludes[$key] = array_merge($prepared_excludes[$key], $rest);
        }
        
        return $prepared_excludes;
    }
    
    public static function checkForParams($prepared_params, $post)
    {
        // предполагаем, что ни один из исключающих параметров не будет найден в теле поста
        $terms = false;
        foreach ($prepared_params as $line) {

            // предполагаем, что в посте есть все искомые исключающие параметры
            $terms_in_line = true;
            foreach ($line as $and_term) {

                $prepared = str_replace(' ', '\s*', $and_term);
                $prepared = str_replace('.', '\.', $and_term);

                // если не находим хотя бы один из них, отмечаем флаг 
                // и прерываем дальнейший поиск по текущей строке параметров
                if (!preg_match("/\b({$prepared})\b/ism", $post)) {

                    $terms_in_line = false;
                    break;
                }
            }

            // если все параметры одной из строк были найдены в посте
            // отмечаем флаг и прерываем поиск остальных параметров
            if ($terms_in_line) {

                $terms = true;
                break;
            }
        }
        
        return $terms;
    }
    
    public static function getCategories()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        // get last session info
        $lastSessionInfo = self::getLastParams();
        
        $categories['last'] = (isset($lastSessionInfo['category']) && !empty($lastSessionInfo['category'])) ? unserialize($lastSessionInfo['category']) : array();
        $categories['list'] = $db->select()->from('category')->order('id')->query()->fetchAll();
        
        return $categories;
    }
}
