<?php

class App_Date extends Zend_Date
{
    public function getDate()
    {
        $date = $this->copyPart(self::DATE_MEDIUM)->setHour(0)->setMinute(0)->setSecond(0);
        return $date;
    }
}
