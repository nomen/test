-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state_id` (`zone_id`),
  KEY `name` (`name`),
  KEY `link` (`link`),
  KEY `fk_state_id` (`zone_id`),
  CONSTRAINT `fk_zone` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,1,'auburn','http://auburn.craigslist.org'),(2,1,'birmingham','http://bham.craigslist.org'),(3,1,'dothan','http://dothan.craigslist.org'),(4,1,'florence / muscle shoals','http://shoals.craigslist.org'),(5,1,'gadsden-anniston','http://gadsden.craigslist.org'),(6,1,'huntsville / decatur','http://huntsville.craigslist.org'),(7,1,'mobile','http://mobile.craigslist.org'),(8,1,'montgomery','http://montgomery.craigslist.org'),(9,1,'tuscaloosa','http://tuscaloosa.craigslist.org'),(473,84,'berlin','http://berlin.craigslist.de'),(474,11,'bremen','http://bremen.craigslist.de'),(475,11,'cologne','http://cologne.craigslist.de'),(476,11,'dresden','http://dresden.craigslist.de'),(477,11,'dusseldorf','http://dusseldorf.craigslist.de'),(478,11,'essen / ruhr','http://essen.craigslist.de'),(479,86,'frankfurt','http://frankfurt.craigslist.de'),(480,11,'hamburg','http://hamburg.craigslist.de'),(481,11,'hannover','http://hannover.craigslist.de'),(482,11,'heidelberg','http://heidelberg.craigslist.de'),(483,11,'kaiserslautern','http://kaiserslautern.craigslist.de'),(484,11,'leipzig','http://leipzig.craigslist.de'),(485,11,'munich','http://munich.craigslist.de'),(486,11,'nuremberg','http://nuremberg.craigslist.de'),(487,11,'stuttgart','http://stuttgart.craigslist.de');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region_id` (`region_id`),
  KEY `name` (`name`),
  KEY `fk_region_id` (`region_id`),
  CONSTRAINT `fk_region_id` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,1,'US'),(53,1,'Canada'),(65,3,'Austria'),(66,3,'Belgium'),(67,3,'Bulgaria'),(68,3,'Croatia'),(69,3,'Czech Republic'),(70,3,'Denmark'),(71,3,'Finland'),(72,3,'France'),(73,3,'Germany'),(74,3,'Greece'),(75,3,'Hungary'),(76,3,'Iceland'),(77,3,'Ireland'),(78,3,'Italy'),(79,3,'Luxembourg'),(80,3,'Netherlands'),(81,3,'Norway'),(82,3,'Poland'),(83,3,'Portugal'),(84,3,'Romania'),(85,3,'Russian Federation'),(86,3,'Spain'),(87,3,'Sweden'),(88,3,'Switzerland'),(89,3,'Turkey'),(90,3,'Ukraine'),(91,3,'United Kingdom'),(92,4,'Bangladesh'),(93,4,'China'),(94,4,'Guam / Micronesia'),(95,4,'Hong Kong'),(96,4,'India'),(97,4,'Indonesia'),(98,4,'Iran'),(99,4,'Iraq'),(100,4,'Israel and Palestine'),(101,4,'Japan'),(102,4,'Korea'),(103,4,'Kuwait'),(104,4,'Lebanon'),(105,4,'Malaysia'),(106,4,'Pakistan'),(107,4,'Philippines'),(108,4,'Singapore'),(109,4,'Taiwan'),(110,4,'Thailand'),(111,4,'United Arab Emirates'),(112,4,'Vietnam'),(113,5,'Australia'),(114,5,'New Zealand'),(115,6,'Argentina'),(116,6,'Bolivia'),(117,6,'Brazil'),(118,6,'Caribbean Islands'),(119,6,'Chile'),(120,6,'Colombia'),(121,6,'Costa Rica'),(122,6,'Dominican Republic'),(123,6,'Ecuador'),(124,6,'El Salvador'),(125,6,'Guatemala'),(126,6,'Mexico'),(127,6,'Nicaragua'),(128,6,'Panama'),(129,6,'Peru'),(130,6,'Puerto Rico'),(131,6,'Uruguay'),(132,6,'Venezuela'),(133,6,'Virgin Islands, U.S.'),(134,7,'Egypt'),(135,7,'Ethiopia'),(136,7,'Ghana'),(137,7,'Kenya'),(138,7,'Morocco'),(139,7,'South Africa'),(140,7,'Tunisia');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object`
--

DROP TABLE IF EXISTS `object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_idx` (`city_id`),
  CONSTRAINT `fk_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object`
--

LOCK TABLES `object` WRITE;
/*!40000 ALTER TABLE `object` DISABLE KEYS */;
INSERT INTO `object` VALUES (1,1,'obj #1'),(2,1,'obj #2'),(3,1,'obj #3'),(4,473,'obj #4'),(5,473,'obj #5'),(6,474,'obj #6'),(7,474,'obj #7'),(8,475,'obj #8'),(9,475,'obj #9'),(10,476,'obj #10'),(11,477,'obj #11'),(12,477,'obj #12'),(13,478,'obj #13'),(14,478,'obj #14'),(15,479,'obj #15'),(16,480,'obj #16'),(17,480,'obj #17'),(18,481,'obj #18'),(19,482,'obj #19'),(20,482,'obj #20'),(21,483,'obj #21'),(22,484,'obj #22'),(23,484,'obj #23'),(24,485,'obj #24'),(25,486,'obj #25'),(26,486,'obj #26'),(27,487,'obj #27'),(28,479,'obj #28'),(29,479,'obj #29'),(30,479,'obj #30');
/*!40000 ALTER TABLE `object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (7,'Africa'),(4,'Asia, Pacific and Middle East'),(3,'Europe'),(6,'Latin America and Caribbean'),(1,'North America'),(5,'Oceania');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `parent_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_country_key_idx` (`country_id`),
  CONSTRAINT `fk_country_key` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,1,0,'SubRegion of US'),(2,53,0,'SubRegion of Canada'),(3,65,0,'SubRegion of Austria'),(4,66,0,'SubRegion of Belgium'),(5,67,0,'SubRegion of Bulgaria'),(6,68,0,'SubRegion of Croatia'),(7,69,0,'SubRegion of Czech Republic'),(8,70,0,'SubRegion of Denmark'),(9,71,0,'SubRegion of Finland'),(10,72,0,'SubRegion of France'),(11,73,0,'SubRegion of Germany #1'),(12,74,0,'SubRegion of Greece'),(13,75,0,'SubRegion of Hungary'),(14,76,0,'SubRegion of Iceland'),(15,77,0,'SubRegion of Ireland'),(16,78,0,'SubRegion of Italy'),(17,79,0,'SubRegion of Luxembourg'),(18,80,0,'SubRegion of Netherlands'),(19,81,0,'SubRegion of Norway'),(20,82,0,'SubRegion of Poland'),(21,83,0,'SubRegion of Portugal'),(22,84,0,'SubRegion of Romania'),(23,85,0,'SubRegion of Russian Federation'),(24,86,0,'SubRegion of Spain'),(25,87,0,'SubRegion of Sweden'),(26,88,0,'SubRegion of Switzerland'),(27,89,0,'SubRegion of Turkey'),(28,90,0,'SubRegion of Ukraine'),(29,91,0,'SubRegion of United Kingdom'),(30,92,0,'SubRegion of Bangladesh'),(31,93,0,'SubRegion of China'),(32,94,0,'SubRegion of Guam / Micronesia'),(33,95,0,'SubRegion of Hong Kong'),(34,96,0,'SubRegion of India'),(35,97,0,'SubRegion of Indonesia'),(36,98,0,'SubRegion of Iran'),(37,99,0,'SubRegion of Iraq'),(38,100,0,'SubRegion of Israel and Palestine'),(39,101,0,'SubRegion of Japan'),(40,102,0,'SubRegion of Korea'),(41,103,0,'SubRegion of Kuwait'),(42,104,0,'SubRegion of Lebanon'),(43,105,0,'SubRegion of Malaysia'),(44,106,0,'SubRegion of Pakistan'),(45,107,0,'SubRegion of Philippines'),(46,108,0,'SubRegion of Singapore'),(47,109,0,'SubRegion of Taiwan'),(48,110,0,'SubRegion of Thailand'),(49,111,0,'SubRegion of United Arab Emirates'),(50,112,0,'SubRegion of Vietnam'),(51,113,0,'SubRegion of Australia'),(52,114,0,'SubRegion of New Zealand'),(53,115,0,'SubRegion of Argentina'),(54,116,0,'SubRegion of Bolivia'),(55,117,0,'SubRegion of Brazil'),(56,118,0,'SubRegion of Caribbean Islands'),(57,119,0,'SubRegion of Chile'),(58,120,0,'SubRegion of Colombia'),(59,121,0,'SubRegion of Costa Rica'),(60,122,0,'SubRegion of Dominican Republic'),(61,123,0,'SubRegion of Ecuador'),(62,124,0,'SubRegion of El Salvador'),(63,125,0,'SubRegion of Guatemala'),(64,126,0,'SubRegion of Mexico'),(65,127,0,'SubRegion of Nicaragua'),(66,128,0,'SubRegion of Panama'),(67,129,0,'SubRegion of Peru'),(68,130,0,'SubRegion of Puerto Rico'),(69,131,0,'SubRegion of Uruguay'),(70,132,0,'SubRegion of Venezuela'),(71,133,0,'SubRegion of Virgin Islands, U.S.'),(72,134,0,'SubRegion of Egypt'),(73,135,0,'SubRegion of Ethiopia'),(74,136,0,'SubRegion of Ghana'),(75,137,0,'SubRegion of Kenya'),(76,138,0,'SubRegion of Morocco'),(77,139,0,'SubRegion of South Africa'),(78,140,0,'SubRegion of Tunisia'),(80,1,1,'SubSubRegion of US'),(81,1,80,'SubSubSubRegion of US'),(82,73,0,'SubRegion of Germany #2'),(83,73,0,'SubRegion of Germany #3'),(84,73,82,'SubRegion of Germany #2.1'),(85,73,82,'SubRegion of Germany #2.2'),(86,73,85,'SubRegion of Germany #2.2.1');
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'test'
--

--
-- Dumping routines for database 'test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-21 16:22:09
