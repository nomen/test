<?php

class IndexController extends Controller_BaseController {

    public function init() {
        parent::init();
    }

    public function indexAction() {

        $db = Zend_Db_Table::getDefaultAdapter();

        /*******************************
         *
         *  1. all objects for Germany
         *
         *******************************/
//        $all_germany = $db->select()
//                     ->from('object', array('object.id', 'object.city_id', 'object.name'))
//                     ->join('city', 'city.id = object.city_id', array('city_name'=>'city.name'))
//                     ->join('zone', 'zone.id = city.zone_id', array('zone_name'=>'zone.name'))
//                     ->join('country', 'country.id = zone.country_id', array('country_name'=>'country.name'))
//                     ->where('country.name = ?', 'Germany')
//                     ->query()
//                     ->fetchAll();
//        var_dump($all_germany); die;

        /*******************************************
         *
         * 2. all objects for any level of sub-region
         *
         *******************************************/

        // array to store all objects that could be found
        $objects = [];

        // get info about the needed sub region
        // in the provided DB dump there are only these sun-regions have objects:
        //   - SubRegion of Germany #1
        //   - SubRegion of Germany #2
        //   - SubRegion of Germany #2.1
        //   - SubRegion of Germany #2.2
        //   - SubRegion of Germany #2.2.1
        //   - SubRegion of US

        $the_sub_region = $db->select()->from('zone')
                             ->where('zone.name = ?', 'SubRegion of Germany #2')
                             ->query()->fetch();

        $zone_to_check = $the_sub_region['id'];
        $objects = $this->processChain($zone_to_check);
        var_dump($objects);

    }

    /**
     * @param mixed $zone_to_check
     *
     * @return mixed
     */
    private function processChain($zone_to_check) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $objects = [];

        do {
            if (!$zone_to_check) {
                break;
            }

            // find an objects in a cities placed exactly in the current sub-region
            $obj_of_region = $this->findObjects($zone_to_check);
            $objects = array_merge($objects, $obj_of_region);

            // try to find any child sub-sub-regions for the current processed region
            $sub_regions = $db->select()->from('zone')
                              ->where('zone.parent_zone_id IN (?)', $zone_to_check)
                              ->query()->fetchAll();

            $sub_IDs = [];
            foreach ($sub_regions as $sRegion) {

                // find an objects in all cities in all child sub-sub-regions
                $obj_of_region = $this->findObjects($sRegion['id']);
                $objects = array_merge($objects, $obj_of_region);

                $sub_IDs[] = $sRegion['id'];
            }

            if (empty($sub_IDs)) {
                break;
            }

            // check if we have even more nested zones
            $next_nested_lvl = $db->select()->from('zone')
                                  ->where('zone.parent_zone_id IN (?)', $sub_IDs)
                                  ->query()->fetchAll();

            if (empty($next_nested_lvl)) {
                break;
            }

            // prepare the next nested level of sub-regions to check (during a next cycle iteration)
            unset($zone_to_check);
            foreach ($next_nested_lvl as $next_zone_to_check) {
                $zone_to_check[] = $next_zone_to_check['id'];
            }
        }
        while(!empty($sub_regions));

        return $objects;
    }

    /**
     * @param int $zone_id
     *
     * @return mixed
     */
    private function findObjects($zone_id) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $obj_of_region = $db->select()
                            ->from('object', array('object.id', 'object.city_id', 'object.name'))
                            ->join('city', 'city.id = object.city_id', array('city_name'=>'city.name'))
                            ->join('zone', 'zone.id = city.zone_id', array('zone_name'=>'zone.name'))
                            ->join('country', 'country.id = zone.country_id', array('country_name'=>'country.name'))
                            ->where('zone.id = ?', $zone_id)
                            ->query()->fetchAll();

        return $obj_of_region;
    }
}
