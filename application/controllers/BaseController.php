<?php

class Controller_BaseController extends Zend_Controller_Action {

    protected $_flashMessenger = null;

    public function init() {

        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->view->headLink()->appendStylesheet('https://cdn.datatables.net/r/dt/jq-2.1.4,dt-1.10.9,af-2.0.0,r-1.0.7/datatables.min.css');
        $this->view->headScript()->appendFile('/scripts/jquery/jquery-2.1.4.min.js');
        $this->view->headScript()->appendFile('https://cdn.datatables.net/r/dt/jq-2.1.4,dt-1.10.9,af-2.0.0,r-1.0.7/datatables.min.js');

        $this->view->headTitle()->setSeparator(' / ');
        $this->view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8');
        $this->view->headTitle()->setDefaultAttachOrder('PREPEND');
        $this->view->headTitle()->append('TEST');
    }
}
