<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initDoctype() {

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }

    protected function _initAutoloader() {

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $resources = new Zend_Loader_Autoloader_Resource(array(
            'basePath'  => APPLICATION_PATH,
            'namespace' => ''
        ));
//        $resources->addResourceType('model', 'models/', 'Model');
//        $resources->addResourceType('form', 'forms/', 'Form');
        $resources->addResourceType('controller', 'controllers/', 'Controller');
        $autoloader->pushAutoloader($resources);
        $autoloader->setFallbackAutoloader(true);

        return $autoloader;
    }

    protected function _initServiceLocator() {

        $this->bootstrap('db');
        $this->bootstrap('locale');
        App_ServiceLocator::store('DatabaseAdapter', Zend_Db_Table::getDefaultAdapter());
    }

    protected function _initConfig() {

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', null, true);
        try {
            $local = new Zend_Config_Ini(APPLICATION_PATH . '/configs/local.ini');
            $config->merge($local);
        }
        catch (Zend_Config_Exception $e) {
        }

        $config->setReadOnly();
        $Registry = Zend_Registry::getInstance();
        $Registry->set('config', $config);
    }

    protected function _initLocale() {

        $locale = new Zend_Locale('en_US');
        Zend_Registry::set('Zend_Locale', $locale);
        Zend_Locale::setDefault($locale);

        return $locale;
    }
}
